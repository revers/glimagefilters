cmake_minimum_required (VERSION 2.6)
 
project (GLImageFilters)

IF(CMAKE_BUILD_TYPE STREQUAL Release) 
    SET(REV_RELEASE_MODE ON)
ELSEIF(CMAKE_BUILD_TYPE STREQUAL Debug) 
	SET(REV_DEBUG_MODE ON)
ENDIF()

IF(REV_RELEASE_MODE) 
	ADD_DEFINITIONS(-DNDEBUG)
ENDIF()

#---------------------------------------------------------------------------
macro (verbose_configure_file src dest) 
	message(STATUS "COPYING (IF NEEDED) FILE ${src} TO ${dest}")
	configure_file("${src}" "${dest}" COPYONLY)
endmacro (verbose_configure_file)
#---------------------------------------------------------------------------
macro (addAllFiles filePattern destDir)
	FILE( GLOB files_temp  ${filePattern})
	foreach(loop_var ${files_temp})
		
		get_filename_component(my_name ${loop_var} NAME)
	
		verbose_configure_file(
			"${loop_var}"
			"${destDir}/${my_name}")
	endforeach(loop_var)
endmacro (addAllFiles)
#---------------------------------------------------------------------------
IF(REV_DEBUG_MODE) 	
	addAllFiles(
		"${PROJECT_SOURCE_DIR}/../RevGL/build/DEBUG/*.dll" 
		"${CMAKE_BINARY_DIR}"
	)
	addAllFiles(
		"${PROJECT_SOURCE_DIR}/../RevCommon/build/DEBUG/*.dll"
		"${CMAKE_BINARY_DIR}"
	)
ELSEIF(REV_RELEASE_MODE)
	addAllFiles(
		"${PROJECT_SOURCE_DIR}/../RevGL/build/RELEASE/*.dll" 
		"${CMAKE_BINARY_DIR}"
	)
	addAllFiles(
		"${PROJECT_SOURCE_DIR}/../RevCommon/build/RELEASE/*.dll"
		"${CMAKE_BINARY_DIR}"
	)
ENDIF()	
#---------------------------------------------------------------------------
addAllFiles(
	"${PROJECT_SOURCE_DIR}/src/shaders/*.glsl"
	"${CMAKE_BINARY_DIR}/shaders" )
#---------------------------------------------------------------------------
addAllFiles(
	"${PROJECT_SOURCE_DIR}/src/shaders/*.vert"
	"${CMAKE_BINARY_DIR}/shaders" )
#---------------------------------------------------------------------------
addAllFiles(
	"${PROJECT_SOURCE_DIR}/src/shaders/*.frag"
	"${CMAKE_BINARY_DIR}/shaders" )
#---------------------------------------------------------------------------
addAllFiles(
	"${PROJECT_SOURCE_DIR}/images/*"
	"${CMAKE_BINARY_DIR}/images" )
#---------------------------------------------------------------------------
addAllFiles(
	"${PROJECT_SOURCE_DIR}/src/*.properties"
	"${CMAKE_BINARY_DIR}")
#---------------------------------------------------------------------------
addAllFiles(
	"${PROJECT_SOURCE_DIR}/*.dll"
	"${CMAKE_BINARY_DIR}")
#---------------------------------------------------------------------------
FILE( GLOB MYFILES src/*.h src/*.cpp src/Filters/*.h src/Filters/*.cpp )
#---------------------------------------------------------------------------
include_directories (
	"${PROJECT_SOURCE_DIR}/../RevGL/include"
	"${PROJECT_SOURCE_DIR}/../include"
	"${PROJECT_SOURCE_DIR}/src/Effects"
	"${PROJECT_SOURCE_DIR}/src/Filters"
    "${PROJECT_SOURCE_DIR}/../RevCommon/include"
)
#---------------------------------------------------------------------------
IF(REV_DEBUG_MODE) 	
	LINK_DIRECTORIES(
		"C:/MinGW/lib"
		"${PROJECT_SOURCE_DIR}/../RevGL/build/DEBUG"
		"${PROJECT_SOURCE_DIR}/../RevCommon/build/DEBUG"
		"${PROJECT_SOURCE_DIR}/../lib"
	) 
	SET(PLATFORM_LIBS boost_filesystem boost_system libRevGLD glew32.dll 
		glfw opengl32 glu32 AntTweakBar log4cplus.dll RevCommonD)
		
ELSEIF(REV_RELEASE_MODE)
	LINK_DIRECTORIES(
		"C:/MinGW/lib"
		"${PROJECT_SOURCE_DIR}/../RevGL/build/RELEASE"
		"${PROJECT_SOURCE_DIR}/../RevCommon/build/RELEASE"
		"${PROJECT_SOURCE_DIR}/../lib"
	) 
	
	SET(PLATFORM_LIBS boost_filesystem boost_system libRevGL glew32.dll 
		glfw opengl32 glu32 AntTweakBar log4cplus.dll RevCommon)
ENDIF()	
#---------------------------------------------------------------------------

add_executable(GLImageFilters ${CONSOLE_SYSTEM} ${MYFILES}) 
TARGET_LINK_LIBRARIES(GLImageFilters ${PLATFORM_LIBS})