/* 
 * File:   WaterRippleFilter.cpp
 * Author: Revers
 * 
 * Created on 24 czerwiec 2012, 13:51
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include "WaterRippleFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>
#include <iostream>

using namespace rev;
using namespace log4cplus;
using namespace std;


#define FILTER_GROUP "Water Ripple"
#define SHADER_FILE "shaders/WaterRippleFilter.glsl"

#define FILTER_PREFIX "WaterRipple"

Logger WaterRippleFilter::logger = Logger::getInstance("filter.WaterRipple");

#define UNIFORM_WAVELENGTH "Wavelength"
#define UNIFORM_RADIUS "Radius"
#define UNIFORM_AMPLITUDE "Amplitude"
#define UNIFORM_PHASE "Phase"
#define UNIFORM_CENTER_RADIUS_SQR "CenterRadiusSqr"
#define UNIFORM_DRAW_CENTER "DrawCenter"
#define UNIFORM_TEX_SIZE "TexSize"
#define UNIFORM_CENTER "Center"

#define DEFAULT_WAVELENGTH 50.0f
#define DEFAULT_RADIUS 230.0f
#define DEFAULT_AMPLITUDE 2.8f
#define DEFAULT_PHASE 0.0f

WaterRippleFilter::WaterRippleFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    radius = DEFAULT_RADIUS;
    wavelength = DEFAULT_WAVELENGTH;
    amplitude = DEFAULT_AMPLITUDE;
    phase = DEFAULT_PHASE;

    centerRadiusSqr = 100.0f;
    drawCenter = true;
    lastMouseX = 0;
    lastMouseY = 0;
    oldWidth = -1.0f;
    oldHeight = -1.0f;
    dragMode = false;
}

bool WaterRippleFilter::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }

    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    program->setUniform(UNIFORM_RADIUS, radius);
    program->setUniform(UNIFORM_WAVELENGTH, wavelength);
    program->setUniform(UNIFORM_AMPLITUDE, amplitude);
    program->setUniform(UNIFORM_PHASE, phase);

    program->setUniform(UNIFORM_CENTER_RADIUS_SQR, centerRadiusSqr);
    program->setUniform(UNIFORM_DRAW_CENTER, drawCenter);

    TwAddVarCB(filterBar, FILTER_PREFIX UNIFORM_WAVELENGTH, TW_TYPE_FLOAT,
            setWavelengthCallback, getWavelengthCallback, this,
            "label='" UNIFORM_WAVELENGTH ":' min=0.1 max=200.0 step=0.01 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, FILTER_PREFIX UNIFORM_RADIUS, TW_TYPE_FLOAT,
            setRadiusCallback, getRadiusCallback, this,
            "label='" UNIFORM_RADIUS ":' min=0.1 max=1000.0 step=0.1 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, FILTER_PREFIX UNIFORM_AMPLITUDE, TW_TYPE_FLOAT,
            setAmplitudeCallback, getAmplitudeCallback, this,
            "label='" UNIFORM_AMPLITUDE ":' min=0.0 max=100.0 step=0.1 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, FILTER_PREFIX UNIFORM_PHASE, TW_TYPE_FLOAT,
            setPhaseCallback, getPhaseCallback, this,
            "label='" UNIFORM_PHASE ":' min=0.0 max=1000.0 step=0.1 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, FILTER_PREFIX UNIFORM_DRAW_CENTER, TW_TYPE_BOOL32,
            setDrawCenterCallback, getDrawCenterCallback, this,
            "label='Draw Center:' group='" FILTER_GROUP "' ");

    addLoadButton(FILTER_PREFIX "Filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void WaterRippleFilter::mouseDragged(int x, int y) {

    if (dragMode) {
        int diffX = x - lastMouseX;
        int diffY = y - lastMouseY;
        center.x += (float) diffX;
        center.y += (float) diffY;

        program->use();
        program->setUniform(UNIFORM_CENTER, center);
    }

    lastMouseX = x;
    lastMouseY = y;
}

void WaterRippleFilter::mouseReleased(int x, int y) {
    if (dragMode) {
        dragMode = false;
    }
}

void WaterRippleFilter::mousePressed(int x, int y) {
    lastMouseX = x;
    lastMouseY = y;

    if (!drawCenter) {
        return;
    }

    float diffX = center.x - (float) x;
    float diffY = center.y - (float) y;

    if (diffX * diffX + diffY * diffY <= centerRadiusSqr) {
        dragMode = true;
    } else {
        dragMode = false;
    }
}

#define MOUSE_WHEEL_STEP 0.05f

void WaterRippleFilter::mouseWheelCallback(int diff) {

    if (diff > 0) {
        wavelength -= MOUSE_WHEEL_STEP;
        if (wavelength < 0.0f) {
            wavelength = 0.0f;
        }
    } else if (diff < 0) {
        wavelength += MOUSE_WHEEL_STEP;
        if (wavelength > 100.0f) {
            wavelength = 100.0f;
        }
    }

    program->setUniform(UNIFORM_WAVELENGTH, wavelength);
}

void WaterRippleFilter::resize(int width, int height) {

    texSize.x = width;
    texSize.y = height;

    if (oldWidth < 0.0f) {
        center = texSize / 2.0f;
    } else {

        float prcX = center.x / oldWidth;
        float prcY = center.y / oldHeight;

        center.x = prcX * texSize.x;
        center.y = prcY * texSize.y;
    }

    oldWidth = (float) width;
    oldHeight = (float) height;

    program->use();

    program->setUniform(UNIFORM_TEX_SIZE, texSize);
    program->setUniform(UNIFORM_CENTER, center);
}

void WaterRippleFilter::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setFilter(this);
}

void WaterRippleFilter::reset() {
    if (!program) {
        return;
    }

    wavelength = DEFAULT_WAVELENGTH;
    radius = DEFAULT_RADIUS;
    amplitude = DEFAULT_AMPLITUDE;
    phase = DEFAULT_PHASE;

    drawCenter = true;

    program->use();
    program->setUniform(UNIFORM_WAVELENGTH, wavelength);
    program->setUniform(UNIFORM_RADIUS, radius);
    program->setUniform(UNIFORM_AMPLITUDE, amplitude);
    program->setUniform(UNIFORM_PHASE, phase);
    program->setUniform(UNIFORM_DRAW_CENTER, drawCenter);
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

const char* WaterRippleFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
WaterRippleFilter::setWavelengthCallback(const void* value, void* clientData) {
    WaterRippleFilter* filter = static_cast<WaterRippleFilter*> (clientData);
    filter->wavelength = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform(UNIFORM_WAVELENGTH, filter->wavelength);
}

void TW_CALL
WaterRippleFilter::getWavelengthCallback(void* value, void* clientData) {
    WaterRippleFilter* filter = static_cast<WaterRippleFilter*> (clientData);

    *(float*) value = filter->wavelength;
}

void TW_CALL
WaterRippleFilter::setRadiusCallback(const void* value, void* clientData) {
    WaterRippleFilter* filter = static_cast<WaterRippleFilter*> (clientData);
    filter->radius = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform(UNIFORM_RADIUS, filter->radius);
}

void TW_CALL
WaterRippleFilter::getRadiusCallback(void* value, void* clientData) {
    WaterRippleFilter* filter = static_cast<WaterRippleFilter*> (clientData);

    *(float*) value = filter->radius;
}

void TW_CALL
WaterRippleFilter::getAmplitudeCallback(void* value, void* clientData) {
    WaterRippleFilter* filter = static_cast<WaterRippleFilter*> (clientData);

    *(float*) value = filter->amplitude;
}

void TW_CALL
WaterRippleFilter::setAmplitudeCallback(const void* value, void* clientData) {
    WaterRippleFilter* filter = static_cast<WaterRippleFilter*> (clientData);
    filter->amplitude = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform(UNIFORM_AMPLITUDE, filter->amplitude);
}

void TW_CALL
WaterRippleFilter::getPhaseCallback(void* value, void* clientData) {
    WaterRippleFilter* filter = static_cast<WaterRippleFilter*> (clientData);

    *(float*) value = filter->phase;
}

void TW_CALL
WaterRippleFilter::setPhaseCallback(const void* value, void* clientData) {
    WaterRippleFilter* filter = static_cast<WaterRippleFilter*> (clientData);
    filter->phase = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform(UNIFORM_PHASE, filter->phase);
}

void TW_CALL
WaterRippleFilter::setDrawCenterCallback(const void* value, void* clientData) {
    WaterRippleFilter* filter = static_cast<WaterRippleFilter*> (clientData);
    filter->drawCenter = *(const bool*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform(UNIFORM_DRAW_CENTER, filter->drawCenter);
}

void TW_CALL
WaterRippleFilter::getDrawCenterCallback(void* value, void* clientData) {
    WaterRippleFilter* filter = static_cast<WaterRippleFilter*> (clientData);

    *(bool*) value = filter->drawCenter;
}