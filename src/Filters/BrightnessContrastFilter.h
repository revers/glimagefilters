/* 
 * File:   BrightnessContrastFilter.h
 * Author: Revers
 *
 * Created on 22 maj 2012, 22:10
 */

#ifndef BRIGHTNESSCONTRASTFILTER_H
#define	BRIGHTNESSCONTRASTFILTER_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class BrightnessContrastFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float brightness;
    float contrast;
public:
    BrightnessContrastFilter(ControlPanel* controlPanel);

    virtual ~BrightnessContrastFilter() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();
private:
    static void TW_CALL setBrightnessCallback(const void* value, void* clientData);
    static void TW_CALL getBrightnessCallback(void* value, void* clientData);

    static void TW_CALL setContrastCallback(const void* value, void* clientData);
    static void TW_CALL getContrastCallback(void* value, void* clientData);
};

#endif	/* BRIGHTNESSCONTRASTFILTER_H */

