/* 
 * File:   Vignette.cpp
 * Author: Revers
 * 
 * Created on 27 maj 2012, 15:48
 */

#include "VignetteFilter.h"

#include <GL/glew.h>
#include <GL/gl.h>
#include "VignetteFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;


#define FILTER_GROUP "Vignette"
#define SHADER_FILE "shaders/VignetteFilter.glsl"

Logger VignetteFilter::logger = Logger::getInstance("filter.Vignette");

#define DEFAULT_AMOUNT 0.0f
#define DEFAULT_SIZE 0.0f

VignetteFilter::VignetteFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    amount = DEFAULT_AMOUNT;
    size = DEFAULT_SIZE;
}

bool VignetteFilter::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }

    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    program->setUniform("amount", amount);

    //(controlPanel->imageWidth, controlPanel->imageHeight);

    TwAddVarCB(filterBar, "vig_amount", TW_TYPE_FLOAT,
            setAmountCallback, getAmountCallback, this,
            "label='Amount:' min=0.0 max=100.0 step=0.01 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, "vig_size", TW_TYPE_FLOAT,
            setSizeCallback, getSizeCallback, this,
            "label='Size:' min=0.0 max=1.0 step=0.1 group='" FILTER_GROUP "' ");

    addLoadButton("vig_Filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void VignetteFilter::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setFilter(this);
}

void VignetteFilter::reset() {
    if (!program) {
        return;
    }

    amount = DEFAULT_AMOUNT;
    size = DEFAULT_SIZE;

    program->use();
    program->setUniform("amount", amount);
    program->setUniform("size", size);
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

const char* VignetteFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
VignetteFilter::setAmountCallback(const void* value, void* clientData) {
    VignetteFilter* filter = static_cast<VignetteFilter*> (clientData);
    filter->amount = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("amount", filter->amount);
}

void TW_CALL
VignetteFilter::getAmountCallback(void* value, void* clientData) {
    VignetteFilter* filter = static_cast<VignetteFilter*> (clientData);

    *(float*) value = filter->amount;
}

void TW_CALL
VignetteFilter::setSizeCallback(const void* value, void* clientData) {
    VignetteFilter* filter = static_cast<VignetteFilter*> (clientData);
    filter->size = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("size", filter->size);
}

void TW_CALL
VignetteFilter::getSizeCallback(void* value, void* clientData) {
    VignetteFilter* filter = static_cast<VignetteFilter*> (clientData);

    *(float*) value = filter->size;
}