/* 
 * File:   CameraFilter.h
 * Author: Revers
 *
 * Created on 15 czerwiec 2012, 20:25
 */

#ifndef CAMERAFILTER_H
#define	CAMERAFILTER_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class CameraFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float time;
    glm::vec2 texSize;
public:
    CameraFilter(ControlPanel* controlPanel);

    virtual ~CameraFilter() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

private:
    static void TW_CALL setTimeCallback(const void* value, void* clientData);
    static void TW_CALL getTimeCallback(void* value, void* clientData);
};

#endif	/* CAMERAFILTER_H */

