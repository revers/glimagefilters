/* 
 * File:   TiltShiftFilter.h
 * Author: Kamil
 *
 * Created on 6 czerwiec 2012, 09:12
 */

#ifndef TILTSHIFTFILTER_H
#define	TILTSHIFTFILTER_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class TiltShiftFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float blurRadius;
    float gradientRadius;
    glm::vec2 texSize;
    glm::vec2 controlPointBegin;
    glm::vec2 controlPointEnd;
    float controlPointsRadiusSqr;

    bool drawControlPoints;
    bool beginDragMode;
    bool endDragMode;

    int lastMouseX;
    int lastMouseY;
    float oldWidth;
    float oldHeight;
public:
    TiltShiftFilter(ControlPanel* controlPanel);

    virtual ~TiltShiftFilter() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();
    virtual void resize(int width, int height);
    virtual void use();
    virtual void apply();

    virtual void mousePressed(int x, int y);
    virtual void mouseDragged(int x, int y);
    virtual void mouseWheelCallback(int diff);
    
private:
    void pass1();
    void pass2();
    
    static void TW_CALL setBlurRadiusCallback(const void* value, void* clientData);
    static void TW_CALL getBlurRadiusCallback(void* value, void* clientData);
    
    static void TW_CALL setGradientRadiusCallback(const void* value, void* clientData);
    static void TW_CALL getGradientRadiusCallback(void* value, void* clientData);

    static void TW_CALL setDrawCenterCallback(const void* value, void* clientData);
    static void TW_CALL getDrawCenterCallback(void* value, void* clientData);
};

#endif	/* TILTSHIFTFILTER_H */

