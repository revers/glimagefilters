/* 
 * File:   DenoiseFilter.h
 * Author: Revers
 *
 * Created on 23 maj 2012, 21:39
 */

#ifndef DENOISEFILTER_H
#define	DENOISEFILTER_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class DenoiseFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float exponent;
    glm::vec2 texSize;
public:
    DenoiseFilter(ControlPanel* controlPanel);

    virtual ~DenoiseFilter() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int width, int height);
    virtual void use();
private:
    static void TW_CALL setExponentCallback(const void* value, void* clientData);
    static void TW_CALL getExponentCallback(void* value, void* clientData);

};

#endif	/* DENOISEFILTER_H */

