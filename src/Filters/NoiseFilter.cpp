/* 
 * File:   NoiseFilter.cpp
 * Author: Revers
 * 
 * Created on 25 maj 2012, 20:28
 */


#include <GL/glew.h>
#include <GL/gl.h>
#include "NoiseFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;


#define FILTER_GROUP "Noise"
#define SHADER_FILE "shaders/NoiseFilter.glsl"

Logger NoiseFilter::logger = Logger::getInstance("filter.Noise");

#define DEFAULT_AMOUNT 0.0f

NoiseFilter::NoiseFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    amount = DEFAULT_AMOUNT;
}

bool NoiseFilter::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }
    
    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    program->setUniform("amount", amount);

    //(controlPanel->imageWidth, controlPanel->imageHeight);

    TwAddVarCB(filterBar, "noi_amount", TW_TYPE_FLOAT,
            setAmountCallback, getAmountCallback, this,
            "label='Amount:' min=0.0 max=100.0 step=0.01 group='" FILTER_GROUP "' ");

    addLoadButton("noi_Filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void NoiseFilter::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setFilter(this);
}

void NoiseFilter::reset() {
    if (!program) {
        return;
    }

    amount = DEFAULT_AMOUNT;

    program->use();
    program->setUniform("amount", amount);
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

const char* NoiseFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
NoiseFilter::setAmountCallback(const void* value, void* clientData) {
    NoiseFilter* filter = static_cast<NoiseFilter*> (clientData);
    filter->amount = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("amount", filter->amount);
}

void TW_CALL
NoiseFilter::getAmountCallback(void* value, void* clientData) {
    NoiseFilter* filter = static_cast<NoiseFilter*> (clientData);

    *(float*) value = filter->amount;
}
