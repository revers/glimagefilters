/* 
 * File:   VibranceFilter.h
 * Author: Revers
 *
 * Created on 25 maj 2012, 21:36
 */

#ifndef VIBRANCEFILTER_H
#define	VIBRANCEFILTER_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class VibranceFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float amount;
    glm::vec2 texSize;
public:
    VibranceFilter(ControlPanel* controlPanel);

    virtual ~VibranceFilter() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void use();
private:
    static void TW_CALL setAmountCallback(const void* value, void* clientData);
    static void TW_CALL getAmountCallback(void* value, void* clientData);
};

#endif	/* VIBRANCEFILTER_H */

