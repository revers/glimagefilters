/* 
 * File:   NoiseFilter.h
 * Author: Revers
 *
 * Created on 25 maj 2012, 20:28
 */

#ifndef NoiseFilter_H
#define	NoiseFilter_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class NoiseFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float amount;
    glm::vec2 texSize;
public:
    NoiseFilter(ControlPanel* controlPanel);

    virtual ~NoiseFilter() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void use();
private:
    static void TW_CALL setAmountCallback(const void* value, void* clientData);
    static void TW_CALL getAmountCallback(void* value, void* clientData);
};

#endif	/* NoiseFilter_H */

