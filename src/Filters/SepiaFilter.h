/* 
 * File:   SepiaFilter.h
 * Author: Revers
 *
 * Created on 25 maj 2012, 21:10
 */

#ifndef SEPIAFILTER_H
#define	SEPIAFILTER_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class SepiaFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float amount;
    glm::vec2 texSize;
public:
    SepiaFilter(ControlPanel* controlPanel);

    virtual ~SepiaFilter() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void use();
private:
    static void TW_CALL setAmountCallback(const void* value, void* clientData);
    static void TW_CALL getAmountCallback(void* value, void* clientData);
};

#endif	/* SEPIAFILTER_H */

