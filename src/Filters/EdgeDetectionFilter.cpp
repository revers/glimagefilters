/* 
 * File:   EdgeDetectionFilter.cpp
 * Author: Revers
 * 
 * Created on 27 maj 2012, 20:39
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include "EdgeDetectionFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;


#define FILTER_GROUP "Edge Detection"
#define SHADER_FILE "shaders/EdgeDetectionFilter.glsl"

Logger EdgeDetectionFilter::logger = Logger::getInstance("filter.EdgeDetection");

#define DEFAULT_THRESHOLD 0.3f

EdgeDetectionFilter::EdgeDetectionFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    threshold = DEFAULT_THRESHOLD;
}

bool EdgeDetectionFilter::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }
    
    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    program->setUniform("EdgeThreshold", threshold);

    //(controlPanel->imageWidth, controlPanel->imageHeight);

    TwAddVarCB(filterBar, "edg_threshold", TW_TYPE_FLOAT,
            setThresholdCallback, getThresholdCallback, this,
            "label='Threshold:' min=0.0 max=100.0 step=0.01 group='" FILTER_GROUP "' ");

    addLoadButton("edg_Filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void EdgeDetectionFilter::resize(int width, int height) {
    program->use();
    program->setUniform("Width", width);
    program->setUniform("Height", height);
}

void EdgeDetectionFilter::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setFilter(this);
}

void EdgeDetectionFilter::reset() {
    if (!program) {
        return;
    }

    threshold = DEFAULT_THRESHOLD;

    program->use();
    program->setUniform("EdgeThreshold", threshold);
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

const char* EdgeDetectionFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
EdgeDetectionFilter::setThresholdCallback(const void* value, void* clientData) {
    EdgeDetectionFilter* filter = static_cast<EdgeDetectionFilter*> (clientData);
    filter->threshold = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("EdgeThreshold", filter->threshold);
}

void TW_CALL
EdgeDetectionFilter::getThresholdCallback(void* value, void* clientData) {
    EdgeDetectionFilter* filter = static_cast<EdgeDetectionFilter*> (clientData);

    *(float*) value = filter->threshold;
}