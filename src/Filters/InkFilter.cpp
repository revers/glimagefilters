/* 
 * File:   InkFilter.cpp
 * Author: Revers
 * 
 * Created on 6 czerwiec 2012, 21:15
 */

#include <GL/glew.h>
#include <GL/gl.h>

#include "InkFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;

#define FILTER_GROUP "Ink"
#define SHADER_FILE "shaders/InkFilter.glsl"

Logger InkFilter::logger = Logger::getInstance("filter.Ink");

#define DEFAULT_STRENGTH 0.0f

InkFilter::InkFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    strength = DEFAULT_STRENGTH;
}

bool InkFilter::init() {
    LOG_TRACE(logger, "init()");
    
    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }
    
    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    program->setUniform("strength", strength);

    //(controlPanel->imageWidth, controlPanel->imageHeight);

    TwAddVarCB(filterBar, "ink_strength", TW_TYPE_FLOAT,
            setStrengthCallback, getStrengthCallback, this,
            "label='Strength:' min=-10.0 max=10.0 step=0.01 group='" FILTER_GROUP "' ");

    addLoadButton("ink_Filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);
    
    return true;
}

void InkFilter::resize(int width, int height) {
    texSize = glm::vec2((float) width, (float) height);
    program->use();
    program->setUniform("texSize", texSize);
}

void InkFilter::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setFilter(this);
}

void InkFilter::reset() {
    if (!program) {
        return;
    }

    strength = DEFAULT_STRENGTH;

    program->use();
    program->setUniform("strength", strength);
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    //   program->setUniform("strength", strength);
}

const char* InkFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
InkFilter::setStrengthCallback(const void* value, void* clientData) {
    InkFilter* filter = static_cast<InkFilter*> (clientData);
    filter->strength = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("strength", filter->strength);

}

void TW_CALL
InkFilter::getStrengthCallback(void* value, void* clientData) {
    InkFilter* filter = static_cast<InkFilter*> (clientData);

    *(float*) value = filter->strength;
}