/* 
 * File:   TriangleBlurFilter.h
 * Author: Revers
 *
 * Created on 28 maj 2012, 07:52
 */

#ifndef TRIANGLEBLURFILTER_H
#define	TRIANGLEBLURFILTER_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class TriangleBlurFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float radius;
    //glm::vec2 texSize;
    glm::vec2 deltaX;
    glm::vec2 deltaY;
public:
    TriangleBlurFilter(ControlPanel* controlPanel);

    virtual ~TriangleBlurFilter() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();
    virtual void apply();
    virtual void use();
    virtual void resize(int width, int height);

    float getRadius() const {
        return radius;
    }

    void setRadius(float radius);
private:
    void pass1();
    void pass2();
    static void TW_CALL setRadiusCallback(const void* value, void* clientData);
    static void TW_CALL getRadiusCallback(void* value, void* clientData);
};

#endif	/* TRIANGLEBLURFILTER_H */

