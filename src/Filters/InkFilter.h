/* 
 * File:   InkFilter.h
 * Author: Revers
 *
 * Created on 6 czerwiec 2012, 21:15
 */

#ifndef INKFILTER_H
#define	INKFILTER_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class InkFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float strength;
    glm::vec2 texSize;
public:
    InkFilter(ControlPanel* controlPanel);

    virtual ~InkFilter() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int width, int height);
    virtual void use();
private:
    static void TW_CALL setStrengthCallback(const void* value, void* clientData);
    static void TW_CALL getStrengthCallback(void* value, void* clientData);

};

#endif	/* INKFILTER_H */

