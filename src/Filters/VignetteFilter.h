/* 
 * File:   Vignette.h
 * Author: Revers
 *
 * Created on 27 maj 2012, 15:48
 */

#ifndef VIGNETTE_H
#define	VIGNETTE_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class VignetteFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float amount;
    float size;
    glm::vec2 texSize;
public:
    VignetteFilter(ControlPanel* controlPanel);

    virtual ~VignetteFilter() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void use();
private:
    static void TW_CALL setAmountCallback(const void* value, void* clientData);
    static void TW_CALL getAmountCallback(void* value, void* clientData);
    
    static void TW_CALL setSizeCallback(const void* value, void* clientData);
    static void TW_CALL getSizeCallback(void* value, void* clientData);
};

#endif	/* VIGNETTE_H */

