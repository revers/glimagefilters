/* 
 * File:   RadialBlurFilter.cpp
 * Author: Revers
 * 
 * Created on 15 czerwiec 2012, 07:54
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include "RadialBlurFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>
#include <iostream>

using namespace rev;
using namespace log4cplus;
using namespace std;


#define FILTER_GROUP "Radial Blur"
#define SHADER_FILE "shaders/RadialBlurFilter.glsl"

Logger RadialBlurFilter::logger = Logger::getInstance("filter.RadialBlur");

#define DEFAULT_TIME 4.5f

RadialBlurFilter::RadialBlurFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    time = DEFAULT_TIME;
    centerRadiusSqr = 100.0f;
    drawCenter = true;
    lastMouseX = 0;
    lastMouseY = 0;
    oldWidth = -1.0f;
    oldHeight = -1.0f;
    dragMode = false;
}

bool RadialBlurFilter::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }

    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    program->setUniform("time", time);
    program->setUniform("centerRadiusSqr", centerRadiusSqr);
    program->setUniform("drawCenter", drawCenter);

    TwAddVarCB(filterBar, "rbl_angle", TW_TYPE_FLOAT,
            setTimeCallback, getTimeCallback, this,
            "label='Angle:' min=-10.0 max=1000.0 step=0.01 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, "rbl_drawCenter", TW_TYPE_BOOL32,
            setDrawCenterCallback, getDrawCenterCallback, this,
            "label='Draw Center:' group='" FILTER_GROUP "' ");

    addLoadButton("rbl_Filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void RadialBlurFilter::mouseDragged(int x, int y) {

    if (dragMode) {
        int diffX = x - lastMouseX;
        int diffY = y - lastMouseY;
        center.x += (float) diffX;
        center.y += (float) diffY;

        program->use();
        program->setUniform("center", center);
    }

    lastMouseX = x;
    lastMouseY = y;
}

void RadialBlurFilter::mouseReleased(int x, int y) {
    if (dragMode) {
        dragMode = false;
    }
}

void RadialBlurFilter::mousePressed(int x, int y) {
    lastMouseX = x;
    lastMouseY = y;

    if (!drawCenter) {
        return;
    }

    float diffX = center.x - (float) x;
    float diffY = center.y - (float) y;

    if (diffX * diffX + diffY * diffY <= centerRadiusSqr) {
        dragMode = true;
    } else {
        dragMode = false;
    }
}

#define MOUSE_WHEEL_STEP 0.05f

void RadialBlurFilter::mouseWheelCallback(int diff) {

    if (diff > 0) {
        time -= MOUSE_WHEEL_STEP;
        if (time < 0.0f) {
            time = 0.0f;
        }
    } else if (diff < 0) {
        time += MOUSE_WHEEL_STEP;
        if (time > 100.0f) {
            time = 100.0f;
        }
    }

    program->setUniform("time", time);
}

void RadialBlurFilter::resize(int width, int height) {

    texSize.x = width;
    texSize.y = height;

    if (oldWidth < 0.0f) {
        center = texSize / 2.0f;
    } else {

        float prcX = center.x / oldWidth;
        float prcY = center.y / oldHeight;

        center.x = prcX * texSize.x;
        center.y = prcY * texSize.y;
    }

    oldWidth = (float) width;
    oldHeight = (float) height;

    program->use();

    program->setUniform("texSize", texSize);
    program->setUniform("center", center);
}

void RadialBlurFilter::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setFilter(this);
}

void RadialBlurFilter::reset() {
    if (!program) {
        return;
    }

    time = DEFAULT_TIME;
    drawCenter = true;

    program->use();
    program->setUniform("time", time);
    program->setUniform("drawCenter", drawCenter);
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

const char* RadialBlurFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
RadialBlurFilter::setTimeCallback(const void* value, void* clientData) {
    RadialBlurFilter* filter = static_cast<RadialBlurFilter*> (clientData);
    filter->time = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("time", filter->time);
}

void TW_CALL
RadialBlurFilter::getTimeCallback(void* value, void* clientData) {
    RadialBlurFilter* filter = static_cast<RadialBlurFilter*> (clientData);

    *(float*) value = filter->time;
}

void TW_CALL
RadialBlurFilter::setDrawCenterCallback(const void* value, void* clientData) {
    RadialBlurFilter* filter = static_cast<RadialBlurFilter*> (clientData);
    filter->drawCenter = *(const bool*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("drawCenter", filter->drawCenter);
}

void TW_CALL
RadialBlurFilter::getDrawCenterCallback(void* value, void* clientData) {
    RadialBlurFilter* filter = static_cast<RadialBlurFilter*> (clientData);

    *(bool*) value = filter->drawCenter;
}