/* 
 * File:   RadialBlurFilter.h
 * Author: Revers
 *
 * Created on 15 czerwiec 2012, 07:54
 */

#ifndef RADIALBLURFILTER_H
#define	RADIALBLURFILTER_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class RadialBlurFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float time;
    glm::vec2 texSize;
    glm::vec2 center;
    float centerRadiusSqr;

    bool drawCenter;
    bool dragMode;

    int lastMouseX;
    int lastMouseY;
    float oldWidth;
    float oldHeight;
public:
    RadialBlurFilter(ControlPanel* controlPanel);

    virtual ~RadialBlurFilter() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();
    virtual void resize(int width, int height);
    virtual void use();

    virtual void mousePressed(int x, int y);
    virtual void mouseReleased(int x, int y);
    virtual void mouseDragged(int x, int y);
    virtual void mouseWheelCallback(int diff);
private:
    static void TW_CALL setTimeCallback(const void* value, void* clientData);
    static void TW_CALL getTimeCallback(void* value, void* clientData);

    static void TW_CALL setDrawCenterCallback(const void* value, void* clientData);
    static void TW_CALL getDrawCenterCallback(void* value, void* clientData);
};

#endif	/* RADIALBLURFILTER_H */

