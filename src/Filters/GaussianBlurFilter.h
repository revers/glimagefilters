/* 
 * File:   GaussianBlurFilter.h
 * Author: Revers
 *
 * Created on 27 maj 2012, 21:35
 */

#ifndef GAUSSIANBLURFILTER_H
#define	GAUSSIANBLURFILTER_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class GaussianBlurFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float sigma2;
    float offsetFactor;
    glm::vec2 texSize;
    GLuint pass1Index;
    GLuint pass2Index;
public:
    GaussianBlurFilter(ControlPanel* controlPanel);

    virtual ~GaussianBlurFilter() {
    }

    virtual bool init();
    virtual void apply();
    virtual void reset();
    virtual const char* getName();
    virtual void resize(int width, int height);
    virtual void use();
private:
    void pass1();
    void pass2();
    void setupGaussian();
    float gauss(float x, float sigma2);
    void setupOffset();
    static void TW_CALL setSigma2Callback(const void* value, void* clientData);
    static void TW_CALL getSigma2Callback(void* value, void* clientData);
    static void TW_CALL setOffsetCallback(const void* value, void* clientData);
    static void TW_CALL getOffsetCallback(void* value, void* clientData);
};

#endif	/* GAUSSIANBLURFILTER_H */

