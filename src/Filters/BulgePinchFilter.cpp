/* 
 * File:   BulgePinchFilter.cpp
 * Author: Kamil
 * 
 * Created on 6 czerwiec 2012, 11:13
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include "BulgePinchFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>
#include <iostream>

using namespace rev;
using namespace log4cplus;
using namespace std;


#define FILTER_GROUP "Bulge / Pinch"
#define SHADER_FILE "shaders/BulgePinchFilter.glsl"

Logger BulgePinchFilter::logger = Logger::getInstance("filter.BulgePinch");

#define DEFAULT_STRENGTH 0.5f
#define DEFAULT_RADIUS 100.0f

BulgePinchFilter::BulgePinchFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    radius = DEFAULT_RADIUS;
    strength = DEFAULT_STRENGTH;
    centerRadiusSqr = 100.0f;
    drawCenter = true;
    lastMouseX = 0;
    lastMouseY = 0;
    oldWidth = -1.0f;
    oldHeight = -1.0f;
    dragMode = false;
}

bool BulgePinchFilter::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }

    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    program->setUniform("radius", radius);
    program->setUniform("strength", strength);
    program->setUniform("centerRadiusSqr", centerRadiusSqr);
    program->setUniform("drawCenter", drawCenter);

    TwAddVarCB(filterBar, "bpnch_strength", TW_TYPE_FLOAT,
            setStrengthCallback, getStrengthCallback, this,
            "label='Strength:' min=-10.0 max=10.0 step=0.01 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, "bpnch_radius", TW_TYPE_FLOAT,
            setRadiusCallback, getRadiusCallback, this,
            "label='Radius:' min=0.0 max=1000.0 step=0.1 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, "bpnch_drawCenter", TW_TYPE_BOOL32,
            setDrawCenterCallback, getDrawCenterCallback, this,
            "label='Draw Center:' group='" FILTER_GROUP "' ");

    addLoadButton("bpnch_Filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void BulgePinchFilter::mouseDragged(int x, int y) {

    if (dragMode) {
        int diffX = x - lastMouseX;
        int diffY = y - lastMouseY;
        center.x += (float) diffX;
        center.y += (float) diffY;

        program->use();
        program->setUniform("center", center);
    }

    lastMouseX = x;
    lastMouseY = y;
}

void BulgePinchFilter::mouseReleased(int x, int y) {
    if (dragMode) {
        dragMode = false;
    }
}

void BulgePinchFilter::mousePressed(int x, int y) {
    lastMouseX = x;
    lastMouseY = y;

    if (!drawCenter) {
        return;
    }

    float diffX = center.x - (float) x;
    float diffY = center.y - (float) y;

    if (diffX * diffX + diffY * diffY <= centerRadiusSqr) {
        dragMode = true;
    } else {
        dragMode = false;
    }
}

#define MOUSE_WHEEL_STEP 0.05f

void BulgePinchFilter::mouseWheelCallback(int diff) {

    if (diff > 0) {
        strength -= MOUSE_WHEEL_STEP;
        if (strength < -10.0f) {
            strength = -10.0f;
        }
    } else if (diff < 0) {
        strength += MOUSE_WHEEL_STEP;
        if (strength > 10.0f) {
            strength = 10.0f;
        }
    }

    program->setUniform("strength", strength);
}

void BulgePinchFilter::resize(int width, int height) {

    texSize.x = width;
    texSize.y = height;

    if (oldWidth < 0.0f) {
        center = texSize / 2.0f;
    } else {

        float prcX = center.x / oldWidth;
        float prcY = center.y / oldHeight;

        center.x = prcX * texSize.x;
        center.y = prcY * texSize.y;
    }

    oldWidth = (float) width;
    oldHeight = (float) height;

    program->use();

    program->setUniform("texSize", texSize);
    program->setUniform("center", center);
}

void BulgePinchFilter::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setFilter(this);
}

void BulgePinchFilter::reset() {
    if (!program) {
        return;
    }

    strength = DEFAULT_STRENGTH;
    radius = DEFAULT_RADIUS;
    drawCenter = true;

    program->use();
    program->setUniform("strength", strength);
    program->setUniform("radius", radius);
    program->setUniform("drawCenter", drawCenter);
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

const char* BulgePinchFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
BulgePinchFilter::setStrengthCallback(const void* value, void* clientData) {
    BulgePinchFilter* filter = static_cast<BulgePinchFilter*> (clientData);
    filter->strength = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("strength", filter->strength);
}

void TW_CALL
BulgePinchFilter::getStrengthCallback(void* value, void* clientData) {
    BulgePinchFilter* filter = static_cast<BulgePinchFilter*> (clientData);

    *(float*) value = filter->strength;
}

void TW_CALL
BulgePinchFilter::setRadiusCallback(const void* value, void* clientData) {
    BulgePinchFilter* filter = static_cast<BulgePinchFilter*> (clientData);
    filter->radius = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("radius", filter->radius);
}

void TW_CALL
BulgePinchFilter::getRadiusCallback(void* value, void* clientData) {
    BulgePinchFilter* filter = static_cast<BulgePinchFilter*> (clientData);

    *(float*) value = filter->radius;
}

void TW_CALL
BulgePinchFilter::setDrawCenterCallback(const void* value, void* clientData) {
    BulgePinchFilter* filter = static_cast<BulgePinchFilter*> (clientData);
    filter->drawCenter = *(const bool*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("drawCenter", filter->drawCenter);
}

void TW_CALL
BulgePinchFilter::getDrawCenterCallback(void* value, void* clientData) {
    BulgePinchFilter* filter = static_cast<BulgePinchFilter*> (clientData);

    *(bool*) value = filter->drawCenter;
}