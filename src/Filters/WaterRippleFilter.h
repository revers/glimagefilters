/* 
 * File:   WaterRippleFilter.h
 * Author: Revers
 *
 * Created on 24 czerwiec 2012, 13:51
 */

#ifndef WATERRIPPLEFILTER_H
#define	WATERRIPPLEFILTER_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class WaterRippleFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float wavelength;
    float radius;
    float amplitude;
    float phase;
    
    glm::vec2 texSize;
    glm::vec2 center;
    float centerRadiusSqr;

    bool drawCenter;
    bool dragMode;

    int lastMouseX;
    int lastMouseY;
    float oldWidth;
    float oldHeight;
public:
    WaterRippleFilter(ControlPanel* controlPanel);

    virtual ~WaterRippleFilter() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();
    virtual void resize(int width, int height);
    virtual void use();

    virtual void mousePressed(int x, int y);
    virtual void mouseReleased(int x, int y);
    virtual void mouseDragged(int x, int y);
    virtual void mouseWheelCallback(int diff);
private:
    static void TW_CALL setWavelengthCallback(const void* value, void* clientData);
    static void TW_CALL getWavelengthCallback(void* value, void* clientData);

    static void TW_CALL setRadiusCallback(const void* value, void* clientData);
    static void TW_CALL getRadiusCallback(void* value, void* clientData);
    
    static void TW_CALL setAmplitudeCallback(const void* value, void* clientData);
    static void TW_CALL getAmplitudeCallback(void* value, void* clientData);

    static void TW_CALL setPhaseCallback(const void* value, void* clientData);
    static void TW_CALL getPhaseCallback(void* value, void* clientData);

    static void TW_CALL setDrawCenterCallback(const void* value, void* clientData);
    static void TW_CALL getDrawCenterCallback(void* value, void* clientData);
};

#endif	/* WATERRIPPLEFILTER_H */

