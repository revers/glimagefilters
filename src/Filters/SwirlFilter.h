/* 
 * File:   SwirlFilter.h
 * Author: Revers
 *
 * Created on 6 czerwiec 2012, 21:01
 */

#ifndef SWIRLFILTER_H
#define	SWIRLFILTER_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class SwirlFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float angle;
    float radius;
    glm::vec2 texSize;
    glm::vec2 center;
    float centerRadiusSqr;

    bool drawCenter;
    bool dragMode;

    int lastMouseX;
    int lastMouseY;
    float oldWidth;
    float oldHeight;
public:
    SwirlFilter(ControlPanel* controlPanel);

    virtual ~SwirlFilter() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();
    virtual void resize(int width, int height);
    virtual void use();

    virtual void mousePressed(int x, int y);
    virtual void mouseReleased(int x, int y);
    virtual void mouseDragged(int x, int y);
    virtual void mouseWheelCallback(int diff);
private:
    static void TW_CALL setAngleCallback(const void* value, void* clientData);
    static void TW_CALL getAngleCallback(void* value, void* clientData);

    static void TW_CALL setRadiusCallback(const void* value, void* clientData);
    static void TW_CALL getRadiusCallback(void* value, void* clientData);

    static void TW_CALL setDrawCenterCallback(const void* value, void* clientData);
    static void TW_CALL getDrawCenterCallback(void* value, void* clientData);
};

#endif	/* SWIRLFILTER_H */

