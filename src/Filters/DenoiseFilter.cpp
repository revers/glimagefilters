/* 
 * File:   DenoiseFilter.cpp
 * Author: Revers
 * 
 * Created on 23 maj 2012, 21:39
 */

#include <GL/glew.h>
#include <GL/gl.h>

#include "DenoiseFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;

#define FILTER_GROUP "Denoise"
#define SHADER_FILE "shaders/DenoiseFilter.glsl"

Logger DenoiseFilter::logger = Logger::getInstance("filter.Denoise");

#define DEFAULT_EXPONENT 0.0f

DenoiseFilter::DenoiseFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    exponent = DEFAULT_EXPONENT;
}

bool DenoiseFilter::init() {
    LOG_TRACE(logger, "init()");
    
    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }
    
    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    program->setUniform("exponent", exponent);

    //(controlPanel->imageWidth, controlPanel->imageHeight);

    TwAddVarCB(filterBar, "den_exponent", TW_TYPE_FLOAT,
            setExponentCallback, getExponentCallback, this,
            "label='Exponent:' min=0.0 max=100.0 step=0.1 group='" FILTER_GROUP "' ");

    addLoadButton("den_Filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);
    
    return true;
}

void DenoiseFilter::resize(int width, int height) {
    texSize = glm::vec2((float) width, (float) height);
    program->use();
    program->setUniform("texSize", texSize);
}

void DenoiseFilter::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setFilter(this);
}

void DenoiseFilter::reset() {
    if (!program) {
        return;
    }

    exponent = DEFAULT_EXPONENT;

    program->use();
    program->setUniform("exponent", exponent);
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    //   program->setUniform("strength", strength);
}

const char* DenoiseFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
DenoiseFilter::setExponentCallback(const void* value, void* clientData) {
    DenoiseFilter* filter = static_cast<DenoiseFilter*> (clientData);
    filter->exponent = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("exponent", filter->exponent);

}

void TW_CALL
DenoiseFilter::getExponentCallback(void* value, void* clientData) {
    DenoiseFilter* filter = static_cast<DenoiseFilter*> (clientData);

    *(float*) value = filter->exponent;
}