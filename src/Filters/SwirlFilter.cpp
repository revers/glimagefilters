/* 
 * File:   SwirlFilter.cpp
 * Author: Revers
 * 
 * Created on 6 czerwiec 2012, 21:01
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include "SwirlFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>
#include <iostream>

using namespace rev;
using namespace log4cplus;
using namespace std;


#define FILTER_GROUP "Swirl"
#define SHADER_FILE "shaders/SwirlFilter.glsl"

Logger SwirlFilter::logger = Logger::getInstance("filter.Swirl");

#define DEFAULT_ANGLE 4.5f
#define DEFAULT_RADIUS 230.0f

SwirlFilter::SwirlFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    radius = DEFAULT_RADIUS;
    angle = DEFAULT_ANGLE;
    centerRadiusSqr = 100.0f;
    drawCenter = true;
    lastMouseX = 0;
    lastMouseY = 0;
    oldWidth = -1.0f;
    oldHeight = -1.0f;
    dragMode = false;
}

bool SwirlFilter::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }

    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    program->setUniform("radius", radius);
    program->setUniform("angle", angle);
    program->setUniform("centerRadiusSqr", centerRadiusSqr);
    program->setUniform("drawCenter", drawCenter);

    TwAddVarCB(filterBar, "swi_angle", TW_TYPE_FLOAT,
            setAngleCallback, getAngleCallback, this,
            "label='Angle:' min=-10.0 max=10.0 step=0.01 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, "swi_radius", TW_TYPE_FLOAT,
            setRadiusCallback, getRadiusCallback, this,
            "label='Radius:' min=0.0 max=1000.0 step=0.1 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, "swi_drawCenter", TW_TYPE_BOOL32,
            setDrawCenterCallback, getDrawCenterCallback, this,
            "label='Draw Center:' group='" FILTER_GROUP "' ");

    addLoadButton("swi_Filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void SwirlFilter::mouseDragged(int x, int y) {

    if (dragMode) {
        int diffX = x - lastMouseX;
        int diffY = y - lastMouseY;
        center.x += (float) diffX;
        center.y += (float) diffY;

        program->use();
        program->setUniform("center", center);
    }

    lastMouseX = x;
    lastMouseY = y;
}

void SwirlFilter::mouseReleased(int x, int y) {
    if (dragMode) {
        dragMode = false;
    }
}

void SwirlFilter::mousePressed(int x, int y) {
    lastMouseX = x;
    lastMouseY = y;

    if (!drawCenter) {
        return;
    }

    float diffX = center.x - (float) x;
    float diffY = center.y - (float) y;

    if (diffX * diffX + diffY * diffY <= centerRadiusSqr) {
        dragMode = true;
    } else {
        dragMode = false;
    }
}

#define MOUSE_WHEEL_STEP 0.05f

void SwirlFilter::mouseWheelCallback(int diff) {

    if (diff > 0) {
        angle -= MOUSE_WHEEL_STEP;
        if (angle < 0.0f) {
            angle = 0.0f;
        }
    } else if (diff < 0) {
        angle += MOUSE_WHEEL_STEP;
        if (angle > 100.0f) {
            angle = 100.0f;
        }
    }

    program->setUniform("angle", angle);
}

void SwirlFilter::resize(int width, int height) {

    texSize.x = width;
    texSize.y = height;

    if (oldWidth < 0.0f) {
        center = texSize / 2.0f;
    } else {

        float prcX = center.x / oldWidth;
        float prcY = center.y / oldHeight;

        center.x = prcX * texSize.x;
        center.y = prcY * texSize.y;
    }

    oldWidth = (float) width;
    oldHeight = (float) height;

    program->use();

    program->setUniform("texSize", texSize);
    program->setUniform("center", center);
}

void SwirlFilter::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setFilter(this);
}

void SwirlFilter::reset() {
    if (!program) {
        return;
    }

    angle = DEFAULT_ANGLE;
    radius = DEFAULT_RADIUS;
    drawCenter = true;

    program->use();
    program->setUniform("angle", angle);
    program->setUniform("radius", radius);
    program->setUniform("drawCenter", drawCenter);
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

const char* SwirlFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
SwirlFilter::setAngleCallback(const void* value, void* clientData) {
    SwirlFilter* filter = static_cast<SwirlFilter*> (clientData);
    filter->angle = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("angle", filter->angle);
}

void TW_CALL
SwirlFilter::getAngleCallback(void* value, void* clientData) {
    SwirlFilter* filter = static_cast<SwirlFilter*> (clientData);

    *(float*) value = filter->angle;
}

void TW_CALL
SwirlFilter::setRadiusCallback(const void* value, void* clientData) {
    SwirlFilter* filter = static_cast<SwirlFilter*> (clientData);
    filter->radius = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("radius", filter->radius);
}

void TW_CALL
SwirlFilter::getRadiusCallback(void* value, void* clientData) {
    SwirlFilter* filter = static_cast<SwirlFilter*> (clientData);

    *(float*) value = filter->radius;
}

void TW_CALL
SwirlFilter::setDrawCenterCallback(const void* value, void* clientData) {
    SwirlFilter* filter = static_cast<SwirlFilter*> (clientData);
    filter->drawCenter = *(const bool*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("drawCenter", filter->drawCenter);
}

void TW_CALL
SwirlFilter::getDrawCenterCallback(void* value, void* clientData) {
    SwirlFilter* filter = static_cast<SwirlFilter*> (clientData);

    *(bool*) value = filter->drawCenter;
}