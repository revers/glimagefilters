/* 
 * File:   UnsharpMaskFilter.cpp
 * Author: Revers
 * 
 * Created on 29 maj 2012, 07:55
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include <iostream>

#include "UnsharpMaskFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;

#define FILTER_GROUP "Unsharp Mask"
#define SHADER_FILE "shaders/UnsharpMaskFilter.glsl"

#define DEFAULT_STRENGTH 0.0f
#define DEFAULT_RADIUS 0.0f

Logger UnsharpMaskFilter::logger = Logger::getInstance("filter.UnsharpMask");

UnsharpMaskFilter::UnsharpMaskFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel), triangleBlurFilter(controlPanel) {
    strength = DEFAULT_STRENGTH;
    radius = DEFAULT_RADIUS;
}

bool UnsharpMaskFilter::init() {
    LOG_TRACE(logger, "init()");

    if (!triangleBlurFilter.init()) {
        LOG_ERROR(logger, "TriangleBlur SUB FILTER init FAILED!!");
        return false;
    }

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }

    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    program->setUniform("strength", strength);
    program->setUniform("Tex1", 0);
    program->setUniform("BlurredTexture", 1);

    TwAddVarCB(filterBar, "uns_strength", TW_TYPE_FLOAT,
            setStrengthCallback, getStrengthCallback, this,
            "label='Strength:' min=0.0 max=100.0 step=0.1 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, "uns_radius", TW_TYPE_FLOAT,
            setRadiusCallback, getRadiusCallback, this,
            "label='Radius:' min=0.0 max=100.0 step=0.1 group='" FILTER_GROUP "' ");

    addLoadButton("uns_filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void UnsharpMaskFilter::apply() {
    
    triangleBlurFilter.apply();
    
    glActiveTexture(GL_TEXTURE1);
    controlPanel->texRenderTarget.getCurrentTexture().bind();
    
    glActiveTexture(GL_TEXTURE0);
    controlPanel->texture.bind();
    
    program->use();
}

void UnsharpMaskFilter::resize(int width, int height) {
    triangleBlurFilter.resize(width, height);
}

void UnsharpMaskFilter::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setFilter(this);
}

void UnsharpMaskFilter::reset() {
    if (!program) {
        return;
    }

    strength = DEFAULT_STRENGTH;
    radius = DEFAULT_RADIUS;

    program->use();
    program->setUniform("strength", strength);
    triangleBlurFilter.setRadius(radius);
}

const char* UnsharpMaskFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
UnsharpMaskFilter::setStrengthCallback(const void* value, void* clientData) {
    UnsharpMaskFilter* filter = static_cast<UnsharpMaskFilter*> (clientData);

    filter->strength = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("strength", filter->strength);

}

void TW_CALL
UnsharpMaskFilter::getStrengthCallback(void* value, void* clientData) {
    UnsharpMaskFilter* filter = static_cast<UnsharpMaskFilter*> (clientData);

    *(float*) value = filter->strength;
}

void TW_CALL
UnsharpMaskFilter::setRadiusCallback(const void* value, void* clientData) {
    UnsharpMaskFilter* filter = static_cast<UnsharpMaskFilter*> (clientData);
    filter->radius = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->triangleBlurFilter.setRadius(filter->radius);
}

void TW_CALL
UnsharpMaskFilter::getRadiusCallback(void* value, void* clientData) {
    UnsharpMaskFilter* filter = static_cast<UnsharpMaskFilter*> (clientData);

    *(float*) value = filter->radius;
}