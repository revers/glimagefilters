/* 
 * File:   DotScreenFilter.cpp
 * Author: Revers
 * 
 * Created on 11 czerwiec 2012, 21:45
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include "DotScreenFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>
#include <iostream>

using namespace rev;
using namespace log4cplus;
using namespace std;


#define FILTER_GROUP "Dot Screen"
#define SHADER_FILE "shaders/DotScreenFilter.glsl"

Logger DotScreenFilter::logger = Logger::getInstance("filter.DotScreen");

#define DEFAULT_ANGLE 0.0f
#define DEFAULT_STRENGTH 0.3f

DotScreenFilter::DotScreenFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    scale = DEFAULT_STRENGTH;
    angle = DEFAULT_ANGLE;
    centerRadiusSqr = 100.0f;
    drawCenter = true;
    lastMouseX = 0;
    lastMouseY = 0;
    oldWidth = -1.0f;
    oldHeight = -1.0f;
    dragMode = false;
}

bool DotScreenFilter::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }

    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    program->setUniform("scale", scale);
    program->setUniform("angle", angle);
    program->setUniform("centerRadiusSqr", centerRadiusSqr);
    program->setUniform("drawCenter", drawCenter);

    TwAddVarCB(filterBar, "dot_scale", TW_TYPE_FLOAT,
            setAngleCallback, getAngleCallback, this,
            "label='Angle:' min=-10.0 max=10.0 step=0.01 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, "dot_angle", TW_TYPE_FLOAT,
            setScaleCallback, getScaleCallback, this,
            "label='Scale:' min=0.0 max=100.0 step=0.01 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, "dot_drawCenter", TW_TYPE_BOOL32,
            setDrawCenterCallback, getDrawCenterCallback, this,
            "label='Draw Center:' group='" FILTER_GROUP "' ");

    addLoadButton("dot_Filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void DotScreenFilter::mouseDragged(int x, int y) {

    if (dragMode) {
        int diffX = x - lastMouseX;
        int diffY = y - lastMouseY;
        center.x += (float) diffX;
        center.y += (float) diffY;

        program->use();
        program->setUniform("center", center);
    }

    lastMouseX = x;
    lastMouseY = y;
}

void DotScreenFilter::mouseReleased(int x, int y) {
    if (dragMode) {
        dragMode = false;
    }
}

void DotScreenFilter::mousePressed(int x, int y) {
    lastMouseX = x;
    lastMouseY = y;

    if (!drawCenter) {
        return;
    }

    float diffX = center.x - (float) x;
    float diffY = center.y - (float) y;

    if (diffX * diffX + diffY * diffY <= centerRadiusSqr) {
        dragMode = true;
    } else {
        dragMode = false;
    }
}

#define MOUSE_WHEEL_STEP 0.05f

void DotScreenFilter::mouseWheelCallback(int diff) {

    if (diff > 0) {
        scale -= MOUSE_WHEEL_STEP;
        if (scale < 0.0f) {
            scale = 0.0f;
        }
    } else if (diff < 0) {
        scale += MOUSE_WHEEL_STEP;
        if (scale > 100.0f) {
            scale = 100.0f;
        }
    }

    program->setUniform("scale", scale);
}

void DotScreenFilter::resize(int width, int height) {

    texSize.x = width;
    texSize.y = height;

    if (oldWidth < 0.0f) {
        center = texSize / 2.0f;
    } else {

        float prcX = center.x / oldWidth;
        float prcY = center.y / oldHeight;

        center.x = prcX * texSize.x;
        center.y = prcY * texSize.y;
    }

    oldWidth = (float) width;
    oldHeight = (float) height;

    program->use();

    program->setUniform("texSize", texSize);
    program->setUniform("center", center);
}

void DotScreenFilter::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setFilter(this);
}

void DotScreenFilter::reset() {
    if (!program) {
        return;
    }

    scale = DEFAULT_STRENGTH;
    angle = DEFAULT_ANGLE;
    drawCenter = true;

    program->use();
    program->setUniform("angle", angle);
    program->setUniform("scale", scale);
    program->setUniform("drawCenter", drawCenter);
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

const char* DotScreenFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
DotScreenFilter::setScaleCallback(const void* value, void* clientData) {
    DotScreenFilter* filter = static_cast<DotScreenFilter*> (clientData);
    filter->scale = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("scale", filter->scale);
}

void TW_CALL
DotScreenFilter::getScaleCallback(void* value, void* clientData) {
    DotScreenFilter* filter = static_cast<DotScreenFilter*> (clientData);

    *(float*) value = filter->scale;
}

void TW_CALL
DotScreenFilter::setAngleCallback(const void* value, void* clientData) {
    DotScreenFilter* filter = static_cast<DotScreenFilter*> (clientData);
    filter->angle = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("angle", filter->angle);
}

void TW_CALL
DotScreenFilter::getAngleCallback(void* value, void* clientData) {
    DotScreenFilter* filter = static_cast<DotScreenFilter*> (clientData);

    *(float*) value = filter->angle;
}

void TW_CALL
DotScreenFilter::setDrawCenterCallback(const void* value, void* clientData) {
    DotScreenFilter* filter = static_cast<DotScreenFilter*> (clientData);
    filter->drawCenter = *(const bool*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("drawCenter", filter->drawCenter);
}

void TW_CALL
DotScreenFilter::getDrawCenterCallback(void* value, void* clientData) {
    DotScreenFilter* filter = static_cast<DotScreenFilter*> (clientData);

    *(bool*) value = filter->drawCenter;
}