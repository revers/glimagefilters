/* 
 * File:   EdgeWorkFilter.cpp
 * Author: Revers
 * 
 * Created on 12 czerwiec 2012, 19:05
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include <iostream>

#include "EdgeWorkFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;

#define FILTER_GROUP "Edge Work"
#define SHADER_FILE "shaders/EdgeWorkFilter.glsl"

#define DEFAULT_RADIUS 2.1f

Logger EdgeWorkFilter::logger = Logger::getInstance("filter.EdgeWork");

EdgeWorkFilter::EdgeWorkFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    radius = DEFAULT_RADIUS;
    pass1Index = 0;
    pass2Index = 0;
}

bool EdgeWorkFilter::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    program->setUniform("Tex1", 0);

    pass1Index = glGetSubroutineIndex(program->getHandle(), GL_FRAGMENT_SHADER, "pass1");
    if (glGetError() == GL_INVALID_VALUE) {
        LOG_ERROR(logger, "Cannot find pass1 subroutine!!");
        return false;
    }
    
    pass2Index = glGetSubroutineIndex(program->getHandle(), GL_FRAGMENT_SHADER, "pass2");
    if (glGetError() == GL_INVALID_VALUE) {
        LOG_ERROR(logger, "Cannot find pass2 subroutine!!");
        return false;
    }

    setRadius(radius);

    TwAddVarCB(filterBar, "edw_radius", TW_TYPE_FLOAT,
            setRadiusCallback, getRadiusCallback, this,
            "label='Radius:' min=0.0 max=100.0 step=0.1 group='" FILTER_GROUP "' ");

    addLoadButton("edw_filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void EdgeWorkFilter::apply() {

    glViewport(0, 0, controlPanel->imageWidth, controlPanel->imageHeight);

    pass1();
    pass2();

    glViewport(0, 0, controlPanel->windowWidth, controlPanel->windowHeight);
}

void EdgeWorkFilter::pass1() {

    controlPanel->texRenderTarget.use();

    glActiveTexture(GL_TEXTURE0);
    controlPanel->texture.bind();

    glClear(GL_COLOR_BUFFER_BIT);

    program->use();
    program->setUniform("delta", deltaA);

    glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &pass1Index);

    controlPanel->secondQuadVBO.render();
}

void EdgeWorkFilter::pass2() {
    controlPanel->texRenderTarget.unuse();

    glActiveTexture(GL_TEXTURE0);
    controlPanel->texRenderTarget.getCurrentTexture().bind();

    program->setUniform("delta", deltaB);

    glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &pass2Index);
}

void EdgeWorkFilter::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setFilter(this);

    setRadius(radius);
}

void EdgeWorkFilter::reset() {
    if (!program) {
        return;
    }

    setRadius(DEFAULT_RADIUS);
}

void EdgeWorkFilter::setRadius(float radius) {
    this->radius = radius;

    deltaA = glm::vec2(radius / controlPanel->imageWidth, 0);
    deltaB = glm::vec2(0, radius / controlPanel->imageHeight);
}

const char* EdgeWorkFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
EdgeWorkFilter::setRadiusCallback(const void* value, void* clientData) {
    EdgeWorkFilter* filter = static_cast<EdgeWorkFilter*> (clientData);
    filter->radius = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->setRadius(filter->radius);
}

void TW_CALL
EdgeWorkFilter::getRadiusCallback(void* value, void* clientData) {
    EdgeWorkFilter* filter = static_cast<EdgeWorkFilter*> (clientData);

    *(float*) value = filter->radius;
}