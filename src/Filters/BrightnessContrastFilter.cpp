/* 
 * File:   BrightnessContrastFilter.cpp
 * Author: Revers
 * 
 * Created on 22 maj 2012, 22:10
 */

#include <GL/glew.h>
#include <GL/gl.h>

#include "BrightnessContrastFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;

#define FILTER_GROUP "Brightness/Contrast"
#define SHADER_FILE "shaders/BrightnessContrastFilter.glsl"

#define DEFAULT_BRIGHTNESS 0.0f
#define DEFAULT_CONTRAST 0.0f

Logger BrightnessContrastFilter::logger = Logger::getInstance("filter.BrightnessContrast");

BrightnessContrastFilter::BrightnessContrastFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    brightness = DEFAULT_BRIGHTNESS;
    contrast = DEFAULT_CONTRAST;
}

bool BrightnessContrastFilter::init() {
    LOG_TRACE(logger, "init()");
    
    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }
    
    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    program->setUniform("brightness", brightness);
    program->setUniform("contrast", contrast);

    TwAddVarCB(filterBar, "bc_brightness", TW_TYPE_FLOAT,
            setBrightnessCallback, getBrightnessCallback, this,
            "label='Brightness:' min=-1.0 max=1.0 step=0.01 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, "bc_contrast", TW_TYPE_FLOAT,
            setContrastCallback, getContrastCallback, this,
            "label='Contrast:' min=-1.0 max=1.0 step=0.01 group='" FILTER_GROUP "' ");

    addLoadButton("bc_filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void BrightnessContrastFilter::reset() {
    if (!program) {
        return;
    }

    brightness = DEFAULT_BRIGHTNESS;
    contrast = DEFAULT_CONTRAST;

    program->use();
    program->setUniform("brightness", brightness);
    program->setUniform("contrast", contrast);
}

const char* BrightnessContrastFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
BrightnessContrastFilter::setBrightnessCallback(const void* value, void* clientData) {
    BrightnessContrastFilter* filter = static_cast<BrightnessContrastFilter*> (clientData);
   
    filter->brightness = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("brightness", filter->brightness);

}

void TW_CALL
BrightnessContrastFilter::getBrightnessCallback(void* value, void* clientData) {
    BrightnessContrastFilter* filter = static_cast<BrightnessContrastFilter*> (clientData);

    *(float*) value = filter->brightness;
}

void TW_CALL
BrightnessContrastFilter::setContrastCallback(const void* value, void* clientData) {
    BrightnessContrastFilter* filter = static_cast<BrightnessContrastFilter*> (clientData);
    filter->contrast = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("contrast", filter->contrast);
}

void TW_CALL
BrightnessContrastFilter::getContrastCallback(void* value, void* clientData) {
    BrightnessContrastFilter* filter = static_cast<BrightnessContrastFilter*> (clientData);

    *(float*) value = filter->contrast;
}

