/* 
 * File:   HueSaturationFilter.h
 * Author: Revers
 *
 * Created on 23 maj 2012, 20:29
 */

#ifndef HUESATURATIONFILTER_H
#define	HUESATURATIONFILTER_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class HueSaturationFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float hue;
    float saturation;
public:
    HueSaturationFilter(ControlPanel* controlPanel);

    virtual ~HueSaturationFilter() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

private:
    static void TW_CALL setHueCallback(const void* value, void* clientData);
    static void TW_CALL getHueCallback(void* value, void* clientData);

    static void TW_CALL setSaturationCallback(const void* value, void* clientData);
    static void TW_CALL getSaturationCallback(void* value, void* clientData);
};

#endif	/* HUESATURATIONFILTER_H */

