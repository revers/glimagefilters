/* 
 * File:   VibranceFilter.cpp
 * Author: Revers
 * 
 * Created on 25 maj 2012, 21:36
 */
#include <GL/glew.h>
#include <GL/gl.h>
#include "VibranceFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;


#define FILTER_GROUP "Vibrance"
#define SHADER_FILE "shaders/VibranceFilter.glsl"

Logger VibranceFilter::logger = Logger::getInstance("filter.Vibrance");

#define DEFAULT_AMOUNT 0.0f

VibranceFilter::VibranceFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    amount = DEFAULT_AMOUNT;
}

bool VibranceFilter::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }
    
    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    program->setUniform("amount", amount);

    //resize(controlPanel->imageWidth, controlPanel->imageHeight);

    TwAddVarCB(filterBar, "vib_amount", TW_TYPE_FLOAT,
            setAmountCallback, getAmountCallback, this,
            "label='Amount:' min=0.0 max=100.0 step=0.01 group='" FILTER_GROUP "' ");

    addLoadButton("vib_Filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void VibranceFilter::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setFilter(this);
}

void VibranceFilter::reset() {
    if (!program) {
        return;
    }

    amount = DEFAULT_AMOUNT;

    program->use();
    program->setUniform("amount", amount);
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

const char* VibranceFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
VibranceFilter::setAmountCallback(const void* value, void* clientData) {
    VibranceFilter* filter = static_cast<VibranceFilter*> (clientData);
    filter->amount = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("amount", filter->amount);
}

void TW_CALL
VibranceFilter::getAmountCallback(void* value, void* clientData) {
    VibranceFilter* filter = static_cast<VibranceFilter*> (clientData);

    *(float*) value = filter->amount;
}