/* 
 * File:   UnsharpMaskFilter.h
 * Author: Revers
 *
 * Created on 29 maj 2012, 07:55
 */

#ifndef UNSHARPMASKFILTER_H
#define	UNSHARPMASKFILTER_H

#include "AbstractFilter.h"
#include "TriangleBlurFilter.h"

namespace log4cplus {
    class Logger;
};

class UnsharpMaskFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float strength;
    float radius;
    TriangleBlurFilter triangleBlurFilter;
public:
    UnsharpMaskFilter(ControlPanel* controlPanel);

    virtual ~UnsharpMaskFilter() {
    }

    virtual void use();
    virtual bool init();
    virtual void reset();
    virtual void resize(int width, int height);
    virtual void apply();
    virtual const char* getName();
private:
    static void TW_CALL setStrengthCallback(const void* value, void* clientData);
    static void TW_CALL getStrengthCallback(void* value, void* clientData);

    static void TW_CALL setRadiusCallback(const void* value, void* clientData);
    static void TW_CALL getRadiusCallback(void* value, void* clientData);
};

#endif	/* UNSHARPMASKFILTER_H */

