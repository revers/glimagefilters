/* 
 * File:   CameraFilter.cpp
 * Author: Revers
 * 
 * Created on 15 czerwiec 2012, 20:25
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include "CameraFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;


#define FILTER_GROUP "Camera"
#define SHADER_FILE "shaders/CameraFilter.glsl"

Logger CameraFilter::logger = Logger::getInstance("filter.Camera");

#define DEFAULT_TIME 0.0f

CameraFilter::CameraFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    time = DEFAULT_TIME;
}

bool CameraFilter::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }
    
    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    program->setUniform("time", time);

    //(controlPanel->imageWidth, controlPanel->imageHeight);

    TwAddVarCB(filterBar, "cmr_amount", TW_TYPE_FLOAT,
            setTimeCallback, getTimeCallback, this,
            "label='Time:' min=0.0 max=100.0 step=0.01 group='" FILTER_GROUP "' ");

    addLoadButton("cmr_Filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void CameraFilter::reset() {
    if (!program) {
        return;
    }

    time = DEFAULT_TIME;

    program->use();
    program->setUniform("time", time);
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

const char* CameraFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
CameraFilter::setTimeCallback(const void* value, void* clientData) {
    CameraFilter* filter = static_cast<CameraFilter*> (clientData);
    filter->time = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("time", filter->time);
}

void TW_CALL
CameraFilter::getTimeCallback(void* value, void* clientData) {
    CameraFilter* filter = static_cast<CameraFilter*> (clientData);

    *(float*) value = filter->time;
}