/* 
 * File:   EdgeDetectionFilter.h
 * Author: Revers
 *
 * Created on 27 maj 2012, 20:39
 */

#ifndef EDGEDETECTIONFILTER_H
#define	EDGEDETECTIONFILTER_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class EdgeDetectionFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float threshold;
    glm::vec2 texSize;
public:
    EdgeDetectionFilter(ControlPanel* controlPanel);

    virtual ~EdgeDetectionFilter() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();
    virtual void resize(int width, int height);
    virtual void use();
private:
    static void TW_CALL setThresholdCallback(const void* value, void* clientData);
    static void TW_CALL getThresholdCallback(void* value, void* clientData);
};

#endif	/* EDGEDETECTIONFILTER_H */

