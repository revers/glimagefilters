/* 
 * File:   EdgeWorkFilter.h
 * Author: Revers
 *
 * Created on 12 czerwiec 2012, 19:05
 */

#ifndef EDGEWORKFILTER_H
#define	EDGEWORKFILTER_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class EdgeWorkFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    
    float radius;
    glm::vec2 deltaA;
    glm::vec2 deltaB;
    
    GLuint pass1Index;
    GLuint pass2Index;
public:
    EdgeWorkFilter(ControlPanel* controlPanel);

    virtual ~EdgeWorkFilter() {
    }

    virtual void use();
    virtual bool init();
    virtual void reset();
    virtual void apply();
    virtual const char* getName();
private:
    void pass1();
    void pass2();
    void setRadius(float radius);
    
    static void TW_CALL setRadiusCallback(const void* value, void* clientData);
    static void TW_CALL getRadiusCallback(void* value, void* clientData);
};

#endif	/* EDGEWORKFILTER_H */

