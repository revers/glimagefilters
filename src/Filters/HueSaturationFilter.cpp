/* 
 * File:   HueSaturationFilter.cpp
 * Author: Revers
 * 
 * Created on 23 maj 2012, 20:29
 */
#include <GL/glew.h>
#include <GL/gl.h>

#include "HueSaturationFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;

#define FILTER_GROUP "Hue/Saturation"
#define SHADER_FILE "shaders/HueSaturationFilter.glsl"

Logger HueSaturationFilter::logger = Logger::getInstance("filter.HueSaturation");

#define DEFAULT_HUE 0.0f
#define DEFAULT_SATURATION 0.0f

HueSaturationFilter::HueSaturationFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    hue = DEFAULT_HUE;
    saturation = DEFAULT_SATURATION;
}

bool HueSaturationFilter::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }
    
    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    program->setUniform("hue", hue);
    program->setUniform("saturation", saturation);

    TwAddVarCB(filterBar, "hs_hue", TW_TYPE_FLOAT,
            setHueCallback, getHueCallback, this,
            "label='Hue:' min=-1.0 max=1.0 step=0.01 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, "hs_saturation", TW_TYPE_FLOAT,
            setSaturationCallback, getSaturationCallback, this,
            "label='Saturation:' min=-1.0 max=1.0 step=0.01 group='" FILTER_GROUP "' ");

    addLoadButton("hs_Filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void HueSaturationFilter::reset() {
    if (!program) {
        return;
    }

    hue = DEFAULT_HUE;
    saturation = DEFAULT_SATURATION;

    program->use();
    program->setUniform("hue", hue);
    program->setUniform("saturation", saturation);
}

const char* HueSaturationFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
HueSaturationFilter::setHueCallback(const void* value, void* clientData) {
    HueSaturationFilter* filter = static_cast<HueSaturationFilter*> (clientData);
    filter->hue = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("hue", filter->hue);

}

void TW_CALL
HueSaturationFilter::getHueCallback(void* value, void* clientData) {
    HueSaturationFilter* filter = static_cast<HueSaturationFilter*> (clientData);

    *(float*) value = filter->hue;
}

void TW_CALL
HueSaturationFilter::setSaturationCallback(const void* value, void* clientData) {
    HueSaturationFilter* filter = static_cast<HueSaturationFilter*> (clientData);
    filter->saturation = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("saturation", filter->saturation);
}

void TW_CALL
HueSaturationFilter::getSaturationCallback(void* value, void* clientData) {
    HueSaturationFilter* filter = static_cast<HueSaturationFilter*> (clientData);

    *(float*) value = filter->saturation;
}