/* 
 * File:   LoggingDefines.h
 * Author: Kamil
 *
 * Created on 25 maj 2012, 09:40
 */

#ifndef LOGGINGDEFINES_H
#define	LOGGINGDEFINES_H

//#define M(X) LOG4CPLUS_TEXT(X)

//#define LOG4CPLUS_DISABLE_ERROR
//#define LOG4CPLUS_DISABLE_WARN
//#define LOG4CPLUS_DISABLE_INFO
//#define LOG4CPLUS_DISABLE_DEBUG
//#define LOG4CPLUS_DISABLE_TRACE

#define LOG_TRACE(logger, logEvent) LOG4CPLUS_TRACE(logger, logEvent)
#define LOG_DEBUG(logger, logEvent) LOG4CPLUS_DEBUG(logger, logEvent)
#define LOG_INFO(logger, logEvent) LOG4CPLUS_INFO(logger, logEvent)
#define LOG_WARN(logger, logEvent) LOG4CPLUS_WARN(logger, logEvent)
#define LOG_ERROR(logger, logEvent) LOG4CPLUS_ERROR(logger, logEvent)
#define LOG_FATAL(logger, logEvent) LOG4CPLUS_FATAL(logger, logEvent)

#endif	/* LOGGINGDEFINES_H */

