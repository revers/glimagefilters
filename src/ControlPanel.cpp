/* 
 * File:   ControlPanel.cpp
 * Author: Revers
 * 
 * Created on 26 kwiecień 2012, 20:33
 */
#include <GL/glew.h>

#include <iostream>
#include <boost/algorithm/string.hpp>

#include "ControlPanel.h"
#include "LoggingDefines.h"
#include "AllFilters.h"
#include "RenderTargetDefines.h"

#include <rev/gl/RevGLUtil.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/common/RevStringUtil.h>

using namespace rev;
using namespace log4cplus;
using namespace boost::filesystem;
using namespace std;

#define X_MIN -0.4f
#define X_MAX 1.0f
#define Y_MIN -1.0f
#define Y_MAX 1.0f
#define FILTER_TOOLBAR_WIDTH 340
#define FILTER_TOOLBAR_HEIGHT 440
#define CONTROL_TOOLBAR_WIDTH FILTER_TOOLBAR_WIDTH
#define CONTROL_TOOLBAR_HEIGHT 145
#define CONTROL_TOOLBAR_POS_X 10
#define CONTROL_TOOLBAR_POS_Y 5
#define FILTER_TOOLBAR_POS_X CONTROL_TOOLBAR_POS_X
#define FILTER_TOOLBAR_POS_Y 158

Logger ControlPanel::logger = Logger::getInstance("ControlPanel");

inline std::string pathToString(const boost::filesystem::path& p) {
    const wchar_t* wFilename = p.c_str();
    return StringUtil::fromWideString(wFilename);
}

ControlPanel::ControlPanel() {

    texture.setInitialInternalFormat(GL_RGBA8);
    texture.setInitialSampler(GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT);
    
    lmbPressed = false;
    currentFilter = NULL;
    currentFilterStr = NULL;
    selectedBackgroundIndex = 0;
    filterBar = NULL;
    mouseOver = false;
    widthOffset = 0;
    lastMouseWheelPos = 0;
}

ControlPanel::~ControlPanel() {
    for (FilterMap::iterator it = filterMap.begin();
            it != filterMap.end(); ++it) {
        delete it->second;
    }
    TwTerminate();
}

bool ControlPanel::init() {
    if (!TwInit(TW_OPENGL_CORE, NULL)) {
        LOG_ERROR(logger, TwGetLastError());
        return false;
    }

    

    if (!quadVBO.create(X_MIN, X_MAX)) {
        LOG_ERROR(logger, "quadVBO.create(X_MIN, X_MAX)) FAILED!!");
        return false;
    }

    

    if (!secondQuadVBO.create()) {
        LOG_ERROR(logger, "secondQuadVBO.create() FAILED!!");
        return false;
    }

    

    TwDefine(" TW_HELP visible=false ");

    controlBar = TwNewBar("Control");
    TwDefine(" Control color='20 20 20' ");
    TwDefine(" Control valueswidth=165 fontSize=3 ");



    filterBar = TwNewBar("Filters");
    TwDefine(" Filters color='20 20 20' ");
    TwDefine(" Filters valueswidth=165 fontSize=3 ");

    TwAddSeparator(controlBar, "__contSep1", NULL);
    if (!loadBackgroundImages()) {
        LOG_ERROR(logger, "ControlPanel::loadBackgroundImages() FAILED!!");
        return false;
    }

    TwAddSeparator(controlBar, "__contSep2", NULL);

    TwCopyCDStringToClientFunc(copyCDStringToClient);
    TwAddVarRO(controlBar, "__currFilter", TW_TYPE_CDSTRING, &currentFilterStr,
            " label='Current Filter:' ");

    TwAddButton(controlBar, "__ResetCurr", resetButtonCallback,
            this, "label='Reset Filter' ");

    TwAddSeparator(controlBar, "__contSep3", NULL);

    if (!addFilters()) {
        LOG_ERROR(logger, "ControlPanel::addFilters() FAILED!!");
        return false;
    }

    struct {
        int x;
        int y;
    } controlPos = {CONTROL_TOOLBAR_POS_X, CONTROL_TOOLBAR_POS_Y};
    TwSetParam(controlBar, NULL, "position", TW_PARAM_INT32, 2, &controlPos);

    struct {
        int x;
        int y;
    } filterPos = {FILTER_TOOLBAR_POS_X, FILTER_TOOLBAR_POS_Y};
    TwSetParam(filterBar, NULL, "position", TW_PARAM_INT32, 2, &filterPos);

    struct {
        int x, y;
    } controlSize = {CONTROL_TOOLBAR_WIDTH, CONTROL_TOOLBAR_HEIGHT};
    TwSetParam(controlBar, NULL, "size", TW_PARAM_INT32, 2, &controlSize);

    struct {
        int x, y;
    } filterSize = {FILTER_TOOLBAR_WIDTH, FILTER_TOOLBAR_HEIGHT};
    TwSetParam(filterBar, NULL, "size", TW_PARAM_INT32, 2, &filterSize);

    return true;
}

void ControlPanel::mousePosCallback(int x, int y) {
    TwEventMousePosGLFW(x, y);

    if (x < widthOffset) {
        mouseOver = true;
    } else {
        mouseOver = false;
    }

    if (!mouseOver && lmbPressed && x >= widthOffset && x < windowWidth
            && y >= 0 && y < windowHeight) {

        currentFilter->mouseDragged(x - widthOffset, imageHeight - y);
    }
}

void ControlPanel::mouseButtonCallback(int id, int state) {
    TwGetParam(filterBar, NULL, "size", TW_PARAM_INT32, 2, &settingsBarBounds.width);
    TwGetParam(filterBar, NULL, "position", TW_PARAM_INT32, 2, &settingsBarBounds.x);

    if (id == GLFW_MOUSE_BUTTON_LEFT) {
        int x, y;
        glfwGetMousePos(&x, &y);

        if (state == GLFW_PRESS) {

            if (!mouseOver && x >= widthOffset && x < windowWidth
                    && y >= 0 && y < windowHeight) {
                currentFilter->mousePressed(x - widthOffset, imageHeight - y);
            }

            lmbPressed = true;
        } else {
            if (lmbPressed) {
                currentFilter->mouseReleased(x - widthOffset, imageHeight - y);
            }

            lmbPressed = false;
            mouseOver = false;


        }
    }

    TwEventMouseButtonGLFW(id, state);
}

void ControlPanel::resizeCallback(int width, int height) {
    this->windowWidth = width;
    this->windowHeight = height;
    this->imageWidth = (int) ((float) width * (X_MAX - X_MIN) / 2.0f);
    this->imageHeight = height;
    this->widthOffset = windowWidth - imageWidth;

    LOG_TRACE(logger, "resizeCallback(" << this->imageWidth << ", " << this->imageHeight << ")");

    if (imageWidth == 0 || imageHeight == 0) {
        return;
    }

    if (!texRenderTarget.create(this->imageWidth, this->imageHeight)) {
        LOG_ERROR(logger, "texRenderTarget.create() FAILED!!");
    }

    if (!secondTexRenderTarget.create(this->imageWidth, this->imageHeight)) {
        LOG_ERROR(logger, "secondTexRenderTarget.create() FAILED!!");
    }

    texRenderTarget.getCurrentTexture().setSampler(
            REV_MIN_FILTER, REV_MAG_FILTER, REV_WRAP_S, REV_WRAP_T);
    secondTexRenderTarget.getCurrentTexture().setSampler(
            REV_MIN_FILTER, REV_MAG_FILTER, REV_WRAP_S, REV_WRAP_T);

    if (currentFilter) {
        currentFilter->resize(this->imageWidth, this->imageHeight);
    }

    TwWindowSize(width, height);
}

bool ControlPanel::addFilters() {
    AbstractFilter* filter = new BrightnessContrastFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "BrightnessContrastFilter::init() FAILED!!");
        delete filter;
    } else {
        filter->use();
        filterMap["brightnessContrast"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new HueSaturationFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "ERROR: HueSaturationFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["hueSaturation"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new NoiseFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "NoiseFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["noise"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new DenoiseFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "DenoiseFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["denoise"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new SepiaFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "SepiaFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["sepia"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new VibranceFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "VibranceFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["vibrance"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new VignetteFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "VignetteFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["vignette"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new EdgeDetectionFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "EdgeDetectionFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["edgeDetection"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new GaussianBlurFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "GaussianBlurFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["gaussianBlur"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new TriangleBlurFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "TriangleBlurFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["triangleBlur"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new UnsharpMaskFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "UnsharpMaskFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["unsharpMask"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new ZoomBlurFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "ZoomBlurFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["zoomBlur"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new TiltShiftFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "TiltShiftFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["tiltShift"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new BulgePinchFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "BulgePinchFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["bulgePinch"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new SwirlFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "SwirlFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["swirl"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new InkFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "InkFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["ink"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new HexagonalPixelateFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "HexagonalPixelateFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["hexagonalPixelate"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new DotScreenFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "DotScreenFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["dotScreen"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new ColorHalftoneFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "ColorHalftoneFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["colorHalftone"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new EdgeWorkFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "EdgeWorkFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["edgeWork"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new LensBlurFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "LensBlurFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["edgeWork"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new RadialBlurFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "RadialBlurFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["radialBlur"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new CameraFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "CameraFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["camera"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new BloomFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "BloomFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["bloom"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new WaterRippleFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "WaterRippleFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["waterRipple"] = filter;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------

    if (filterMap.empty()) {
        return false;
    }

    return true;
}

void ControlPanel::render() {
    if (!currentFilter || texture.isInitialized() == false) {
        TwDraw();
        return;
    }

    currentFilter->apply();
    quadVBO.render();

    TwDraw();
}

bool ControlPanel::loadBackgroundImages() {
    if (!getImagePathList("images/", backgroudImagesVector)) {
        return false;
    }

    if (backgroudImagesVector.size() == 0) {
        LOG_ERROR(logger, "There are no image files in 'images' direcotry!!");
        return false;
    }

    size_t size = backgroudImagesVector.size();

    TwEnumVal* imagesEnum = new TwEnumVal[size];
    string** filenames = new string*[size];
    int i = 0;

    for (vector<path>::iterator iter = backgroudImagesVector.begin();
            iter != backgroudImagesVector.end();) {
        path p = *(iter++);

        imagesEnum[i].Value = i;


        filenames[i] = new string(pathToString(p));
        imagesEnum[i].Label = filenames[i]->c_str();
        i++;
    }


    TwType imagesType = TwDefineEnum("BackgroundType", imagesEnum, size);
    TwAddVarCB(controlBar, "Background", imagesType, setBackgroundCallback,
            getBackgroundCallback, this, "label='Image:' ");

    for (int i = 0; i < size; i++) {
        delete filenames[i];
    }

    delete[] imagesEnum;
    delete[] filenames;

    string filename = pathToString(backgroudImagesVector[0]); //.file_string();

    if (!changeTexture(filename.c_str())) {
        return false;
    }

    return true;
}

bool ControlPanel::getImagePathList(const char* directory, vector<path>& result) {
    path p(directory);

    static string extensions[] = {".bmp", ".tga", ".png", ".jpg", ".jpeg"};
    static size_t extensionsSize = sizeof (extensions) / sizeof (string);

    try {
        if (exists(p)) {

            if (is_directory(p)) {

                for (directory_iterator iter = directory_iterator(p);
                        iter != directory_iterator();) {
                    path p1 = *(iter++);

                    if (!is_regular_file(p1)) {
                        continue;
                    }

                    for (int i = 0; i < extensionsSize; i++) {
                        path extPath = p1.extension();
                        const wchar_t* wExt = extPath.c_str();
                        string ext = StringUtil::fromWideString(wExt);
                        const char* ext1 = extensions[i].c_str();
                        const char* ext2 = ext.c_str();
                        if (strcmp(ext1, ext2) == 0) {
                            result.push_back(p1);
                            break;
                        }
                    }
                }
            } else {
                LOG_ERROR(logger, "'" << p << "' is not a directory!!");
                return false;
            }

        } else {
            LOG_ERROR(logger, "'" << p << "' does not exist!!");
            return false;
        }
    } catch (const filesystem_error& ex) {
        LOG_ERROR(logger, ex.what());
        return false;
    }

    return true;
}

void TW_CALL ControlPanel::setBackgroundCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->selectedBackgroundIndex = *(const int*) value;
    string filename = pathToString(controlPanel->backgroudImagesVector[controlPanel->selectedBackgroundIndex]);

    controlPanel->changeTexture(filename.c_str());
}

void TW_CALL ControlPanel::getBackgroundCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(int*) value = controlPanel->selectedBackgroundIndex;
}

void TW_CALL ControlPanel::resetButtonCallback(void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    LOG_TRACE(logger, "Reset current filter ");

    if (controlPanel->currentFilter != NULL) {
        controlPanel->currentFilter->reset();
    }
}

bool ControlPanel::changeTexture(const char* textureFile) {

    texture.load(textureFile);

    if (texture.isInitialized() == false) {
        LOG_ERROR(logger, "Loading texture '" << textureFile << "' FAILED!");
        return false;
    }

    LOG_INFO(logger, "Texture '" << textureFile << "' loaded successfully.");

    return true;
}

// ---------------------------------------------------------------------------
// 2) Callback functions for C-Dynamic string variables
// ---------------------------------------------------------------------------

// Function called to copy the content of a C-Dynamic String (src) handled by
// the AntTweakBar library to a C-Dynamic string (*destPtr) handled by our application

void TW_CALL ControlPanel::copyCDStringToClient(char** destPtr, const char* src) {
    size_t srcLen = (src != NULL) ? strlen(src) : 0;
    size_t destLen = (*destPtr != NULL) ? strlen(*destPtr) : 0;

    // Alloc or realloc dest memory block if needed
    if (*destPtr == NULL)
        *destPtr = (char *) malloc(srcLen + 1);
    else if (srcLen > destLen)
        *destPtr = (char *) realloc(*destPtr, srcLen + 1);

    // Copy src
    if (srcLen > 0)
        strncpy(*destPtr, src, srcLen);
    (*destPtr)[srcLen] = '\0'; // null-terminated string
}

void ControlPanel::setFilter(AbstractFilter* filter) {
    this->currentFilter = filter;
    copyCDStringToClient(&currentFilterStr, filter->getName());

    LOG_INFO(logger, "Using filter: " << currentFilter->getName());

    TwRefreshBar(controlBar);
}

