/* 
 * File:   ControlPanel.h
 * Author: Revers
 *
 * Created on 26 kwiecień 2012, 20:33
 */

#ifndef CONTROLPANEL_H
#define	CONTROLPANEL_H

#include <boost/filesystem.hpp>
#include "Filters/AbstractFilter.h"

#include <GL/glfw.h>
#include <AntTweakBar.h>

#include <vector>
#include <map>

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#include <rev/gl/RevVBOQuadVT.h>
#include <rev/gl/RevGLTextureRenderTarget.h>
#include <rev/gl/RevGLTexture2D.h>

typedef std::map<std::string, AbstractFilter*> FilterMap;

class ControlPanel {
    static log4cplus::Logger logger;
    typedef std::vector<boost::filesystem::path> PathVector;

    friend class AbstractFilter;

    struct TweakBarBounds {
        int x;
        int y;
        int width;
        int height;
    };

    TweakBarBounds settingsBarBounds;
    TwBar *filterBar;
    TwBar *controlBar;

    AbstractFilter* currentFilter;

    PathVector backgroudImagesVector;
    int selectedBackgroundIndex;

    char* currentFilterStr;
    bool lmbPressed;

    int lastMouseWheelPos;
public:
    FilterMap filterMap;

    rev::GLTexture2D texture;
    rev::VBOQuadVT quadVBO;
    rev::VBOQuadVT secondQuadVBO;

    rev::GLTextureRenderTarget texRenderTarget;
    rev::GLTextureRenderTarget secondTexRenderTarget;
    int imageWidth;
    int imageHeight;
    int windowWidth;
    int windowHeight;
    int widthOffset;
    bool mouseOver;

    ControlPanel();

    virtual ~ControlPanel();

    void render();

    bool init();

    const char* getCurrentFilterName() {
        if (!currentFilter) {
            return NULL;
        }
        return currentFilter->getName();
    }

    void setFilter(AbstractFilter* filter);

    AbstractFilter* getFilter() {
        return currentFilter;
    }

    void keyDownCallback(int key, int action) {
        TwEventKeyGLFW(key, action);
    }

    void resizeCallback(int width, int height);

    void mousePosCallback(int x, int y);

    void mouseButtonCallback(int id, int state);

    void mouseWheelCallback(int pos) {
        int diff = pos - lastMouseWheelPos;
        lastMouseWheelPos = pos;

        if (!mouseOver) {
            currentFilter->mouseWheelCallback(diff);
        }

        refresh();
        TwEventMouseWheelGLFW(pos);
    }

    void refresh() {
        TwRefreshBar(filterBar);
        TwRefreshBar(controlBar);
    }

private:
    bool changeTexture(const char* textureFile);

    static void TW_CALL setBackgroundCallback(const void* value, void* clientData);
    static void TW_CALL getBackgroundCallback(void* value, void* clientData);

    static void TW_CALL resetButtonCallback(void* clientData);
    static void TW_CALL copyCDStringToClient(char** destPtr, const char* src);


    bool loadBackgroundImages();

    bool getImagePathList(const char* directory,
            std::vector<boost::filesystem::path>& result);

    bool addFilters();
};

#endif	/* CONTROLPANEL_H */

