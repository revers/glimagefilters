
#include <FAKE_INCLUDE/glsl_fake.hxx>

//$END_FAKE$

//-- Vertex
#version 150

in vec3 VertexPosition;
in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition, 1.0);
}

/**
 * 
 * Fragment shader from glfx.js (http://evanw.github.com/glfx.js/) by Evan Wallace.
 * 
 * @filter         Ink
 * @description    Simulates outlining the image in ink by darkening edges stronger than a
 *                 certain threshold. The edge detection value is the difference of two
 *                 copies of the image, each blurred using a blur of a different radius.
 * @param strength The multiplicative scale of the ink edges. Values in the range 0 to 1
 *                 are usually sufficient, where 0 doesn't change the image and 1 adds lots
 *                 of black edges. Negative strength values will create white ink edges
 *                 instead of black ones.
 */
//-- Fragment
#version 150

in vec2 TexCoord;

uniform sampler2D Tex1;
uniform float strength;
uniform vec2 TexSize = vec2(800, 600);

out vec4 FragColor;

void main() {
    vec2 dx = vec2(1.0 / TexSize.x, 0.0);
    vec2 dy = vec2(0.0, 1.0 / TexSize.y);
    vec4 color = texture(Tex1, TexCoord);
    float bigTotal = 0.0;
    float smallTotal = 0.0;
    vec3 bigAverage = vec3(0.0);
    vec3 smallAverage = vec3(0.0);
    for (float x = -2.0; x <= 2.0; x += 1.0) {
        for (float y = -2.0; y <= 2.0; y += 1.0) {
            vec3 sample = texture(Tex1, TexCoord + dx * x + dy * y).rgb;
            bigAverage += sample;
            bigTotal += 1.0;
            if (abs(x) + abs(y) < 2.0) {
                smallAverage += sample;
                smallTotal += 1.0;
            }
        }
    }
    vec3 edge = max(vec3(0.0), bigAverage / bigTotal - smallAverage / smallTotal);
    FragColor = vec4(color.rgb - dot(edge, edge) * strength * 100000.0, color.a);
}




