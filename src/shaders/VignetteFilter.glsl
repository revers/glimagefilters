#include <FAKE_INCLUDE/glsl_fake.hxx>

//$END_FAKE$

//-- Vertex
#version 150

in vec3 VertexPosition;
in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition, 1.0);
}

/**
 * 
 * Fragment shader from glfx.js (http://evanw.github.com/glfx.js/) by Evan Wallace.
 * 
 * @filter         Vignette
 * @description    Adds a simulated lens edge darkening effect.
 * @param size     0 to 1 (0 for center of frame, 1 for edge of frame)
 * @param amount   0 to 1 (0 for no effect, 1 for maximum lens darkening)
 */
//-- Fragment
#version 150

in vec2 TexCoord;

uniform sampler2D Tex1;

uniform float size;
uniform float amount;

out vec4 FragColor;

void main() {
    vec4 color = texture(Tex1, TexCoord);

    float dist = distance(TexCoord, vec2(0.5, 0.5));
    color.rgb *= smoothstep(0.8, size * 0.799, dist * (amount + size));

    FragColor = color;
}



