
#include <FAKE_INCLUDE/glsl_fake.hxx>

//$END_FAKE$

//-- Vertex
#version 150

in vec3 VertexPosition;
in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition, 1.0);
}

/**
 * 
 * Fragment shader from glfx.js (http://evanw.github.com/glfx.js/) by Evan Wallace.
 * 
 * Fragment shader a bit modified taken from "Shader Toy - Radial Blur" 
 * by Inigo Quilez and others (http://www.iquilezles.org/apps/shadertoy/)
 * 
 */
//-- Fragment
#version 150

in vec2 TexCoord;

///uniform vec2 resolution;
///uniform float time;
uniform sampler2D Tex1;
uniform float time;
uniform vec2 texSize; // = vec2(800, 600);

uniform vec2 center;

uniform bool drawCenter = true;
uniform float centerRadiusSqr = 10.0f * 10.0f;

out vec4 FragColor;

vec3 deform(vec2 p, vec2 offset) {
    vec2 uv;

    vec2 q = vec2(sin(1.1 * time + p.x), sin(1.2 * time + p.y));

    //float a = atan(q.y, q.x);
    float r = sqrt(dot(q, q));

    uv.x = sin(0.0 + 1.0 * time) + p.x * sqrt(r * r + 1.0);
    uv.y = sin(0.6 + 1.1 * time) + p.y * sqrt(r * r + 1.0);

    return texture(Tex1, uv * .5 - offset).xyz;
}

void main() {
    vec2 coord = TexCoord * texSize;
    vec2 toCenter = center - coord;

    if (drawCenter && dot(toCenter, toCenter) <= centerRadiusSqr) {
        FragColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
        return;
    }
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // vec2 p = -1.0 + 2.0 * gl_FragCoord.xy / texSize.xy;
    // vec2 s = p;
    
    vec2 offset = center / texSize.xy - vec2(0.5);
    
    vec2 p = -1.0 + 2.0 * TexCoord - offset;
    // more symetric:
    //vec2 p = -1.0 + 2.0 * TexCoord - (-1.0 + 2.0 * center / texSize);
    vec2 s = p;// -  (-1.0 + 2.0 * center / texSize);;

    vec3 total = vec3(0.0);
    vec2 d = (vec2(0.0, 0.0) - p) / 40.0;
    float w = 1.0;
    for (int i = 0; i < 40; i++) {
        vec3 res = deform(s, offset);
        res = smoothstep(0.1, 1.0, res * res);
        total += w*res;
        w *= .99;
        s += d;
    }
    total /= 40.0;
    float r = 1.5 / (1.0 + dot(p, p));
    FragColor = vec4(total*r, 1.0);
}



