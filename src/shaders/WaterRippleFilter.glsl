#include <FAKE_INCLUDE/glsl_fake.hxx>

//$END_FAKE$

//-- Vertex
#version 150

in vec3 VertexPosition;
in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition, 1.0);
}

//-- Fragment
#version 150

#define TWO_PI 6.2831855

in vec2 TexCoord;
out vec4 FragColor;

uniform sampler2D Tex1;

uniform float Radius = 50.0f;
uniform float Wavelength;
uniform float Phase;
uniform float Amplitude;
uniform vec2 Center;
uniform vec2 TexSize;

uniform bool DrawCenter = true;
uniform float CenterRadiusSqr = 10.0f * 10.0f;

void main() {
    vec2 coord = TexCoord * TexSize; // x, y
    vec2 toCenter = coord - Center; // dx, dy

    if (DrawCenter && dot(toCenter, toCenter) <= CenterRadiusSqr) {
        FragColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
        return;
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~
    //coord -= Center;
    float distance = length(toCenter);
    if (distance > Radius) {
        FragColor = texture(Tex1, TexCoord);
    } else {
      //  float percent = (Radius - distance) / Radius;
        float amount = Amplitude * sin(distance / Wavelength * TWO_PI - Phase);
        amount *= (Radius - distance) / Radius;
        if (distance != 0) {
            amount *= Wavelength / distance;
        }

        FragColor = texture(Tex1, (coord + toCenter * amount) / TexSize);
        //            out[0] = x + dx * amount;
        //            out[1] = y + dy * amount;
    }

    //FragColor = texture(Tex1, coord / TexSize);

}

