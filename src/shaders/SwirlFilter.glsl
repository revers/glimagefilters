#include <FAKE_INCLUDE/glsl_fake.hxx>

//$END_FAKE$

//-- Vertex
#version 150

in vec3 VertexPosition;
in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition, 1.0);
}

/**
 * 
 * Fragment shader from glfx.js (http://evanw.github.com/glfx.js/) by Evan Wallace.
 * 
 * @filter        Swirl
 * @description   Warps a circular region of the image in a swirl.
 * @param centerX The x coordinate of the center of the circular region.
 * @param centerY The y coordinate of the center of the circular region.
 * @param radius  The radius of the circular region.
 * @param angle   The angle in radians that the pixels in the center of
 *                the circular region will be rotated by.
 */
//-- Fragment
#version 150

in vec2 TexCoord;
out vec4 FragColor;

uniform sampler2D Tex1;

uniform float radius = 50.0f;
uniform float angle;
uniform vec2 center;
uniform vec2 texSize;

uniform bool drawCenter = true;
uniform float centerRadiusSqr = 10.0f * 10.0f;

void main() {
    vec2 coord = TexCoord * texSize;
    vec2 toCenter = center - coord;

    if (drawCenter && dot(toCenter, toCenter) <= centerRadiusSqr) {
        FragColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
        return;
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~
    coord -= center;
    float distance = length(coord);
    if (distance < radius) {
        float percent = (radius - distance) / radius;
        float theta = percent * percent * angle;
        float s = sin(theta);
        float c = cos(theta);
        coord = vec2(
                coord.x * c - coord.y * s,
                coord.x * s + coord.y * c
                );
    }
    coord += center;
    //--------------------------

    FragColor = texture(Tex1, coord / texSize);
    vec2 clampedCoord = clamp(coord, vec2(0.0), texSize);
    if (coord != clampedCoord) {
        /* fade to transparent if we are outside the image */
        FragColor.a *= max(0.0, 1.0 - length(coord - clampedCoord));
    }
}
