#include <FAKE_INCLUDE/glsl_fake.hxx>

//$END_FAKE$

//-- Vertex
#version 150

in vec3 VertexPosition;
in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition, 1.0);
}

/**
 * Fragment shader from glfx.js (http://evanw.github.com/glfx.js/) by Evan Wallace.
 * 
 * @filter        Color Halftone
 * @description   Simulates a CMYK halftone rendering of the image by multiplying pixel values
 *                with a four rotated 2D sine wave patterns, one each for cyan, magenta, yellow,
 *                and black.
 * @param centerX The x coordinate of the pattern origin.
 * @param centerY The y coordinate of the pattern origin.
 * @param angle   The rotation of the pattern in radians.
 * @param size    The diameter of a dot in pixels.
 */
//-- Fragment
#version 150

in vec2 TexCoord;
out vec4 FragColor;

uniform sampler2D Tex1;

uniform float angle;
uniform float scale;
uniform vec2 center;
uniform vec2 texSize;

//----------------------------
uniform bool drawCenter = true;
uniform float centerRadiusSqr = 10.0f * 10.0f;
//============================

float pattern(float angle, vec2 tex) {
    float s = sin(angle), c = cos(angle);
    //vec2 tex = texCoord * texSize - center;
    vec2 point = vec2(
            c * tex.x - s * tex.y,
            s * tex.x + c * tex.y
            ) * scale;
    return (sin(point.x) * sin(point.y)) * 4.0;
}

void main() {
    vec2 coord = TexCoord * texSize - center;

    if (drawCenter && dot(coord, coord) <= centerRadiusSqr) {
        FragColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
        return;
    }
    
    vec4 color = texture(Tex1, TexCoord);
    vec3 cmy = 1.0 - color.rgb;
    float k = min(cmy.x, min(cmy.y, cmy.z));
    cmy = (cmy - k) / (1.0 - k);
    cmy = clamp(cmy * 10.0 - 3.0 + vec3(
            pattern(angle + 0.26179, coord),
            pattern(angle + 1.30899, coord), 
            pattern(angle, coord)), 0.0, 1.0);
    
    k = clamp(k * 10.0 - 5.0 + pattern(angle + 0.78539, coord), 0.0, 1.0);
    FragColor = vec4(1.0 - cmy - k, color.a);
}
