
#include <FAKE_INCLUDE/glsl_fake.hxx>

//$END_FAKE$

//-- Vertex
#version 150

in vec3 VertexPosition;
in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition,1.0);
}

/**
 * Fragment shader from glfx.js (http://evanw.github.com/glfx.js/) by Evan Wallace.
 * 
 * @filter           Brightness / Contrast
 * @description      Provides additive brightness and multiplicative contrast control.
 * @param brightness -1 to 1 (-1 is solid black, 0 is no change, and 1 is solid white)
 * @param contrast   -1 to 1 (-1 is solid gray, 0 is no change, and 1 is maximum contrast)
 */
//-- Fragment
#version 150

in vec2 TexCoord;

uniform sampler2D Tex1;
uniform float brightness;
uniform float contrast;

out vec4 FragColor;

void main() {
    vec4 color = texture(Tex1, TexCoord);
    color.rgb += brightness;
    if (contrast > 0.0) {
        color.rgb = (color.rgb - 0.5) / (1.0 - contrast) + 0.5;
    } else {
        color.rgb = (color.rgb - 0.5) * (1.0 + contrast) + 0.5;
    }
    
    FragColor = color;
}

