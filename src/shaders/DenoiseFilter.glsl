
#include <FAKE_INCLUDE/glsl_fake.hxx>

//$END_FAKE$

//-- Vertex
#version 150

in vec3 VertexPosition;
in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition, 1.0);
}

/**
 * 
 * Fragment shader from glfx.js (http://evanw.github.com/glfx.js/) by Evan Wallace.
 * 
 * @filter         Denoise
 * @description    Smooths over grainy noise in dark images using an 9x9 box filter
 *                 weighted by color intensity, similar to a bilateral filter.
 * @param exponent The exponent of the color intensity difference, should be greater
 *                 than zero. A value of zero just gives an 9x9 box blur and high values
 *                 give the original image, but ideal values are usually around 10-20.
 */
//-- Fragment
#version 150

in vec2 TexCoord;

uniform sampler2D Tex1;
uniform float exponent;
uniform vec2 TexSize = vec2(800, 600);

out vec4 FragColor;

void main() {
    vec4 center = texture(Tex1, TexCoord);

    vec4 color = vec4(0.0);
    float total = 0.0;
    for (float x = -4.0; x <= 4.0; x += 1.0) {
        for (float y = -4.0; y <= 4.0; y += 1.0) {
            vec2 coord = TexCoord + vec2(x, y) / TexSize;
            vec4 sample1 = texture(Tex1, coord);
            float weight = 1.0 - abs(dot(sample1.rgb - center.rgb, vec3(0.25)));
            weight = pow(weight, exponent);
            color += sample1 * weight;
            total += weight;
        }
    }
    FragColor = color / total;
}



