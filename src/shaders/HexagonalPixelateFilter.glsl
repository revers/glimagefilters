#include <FAKE_INCLUDE/glsl_fake.hxx>

//$END_FAKE$

//-- Vertex
#version 150

in vec3 VertexPosition;
in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition, 1.0);
}

/**
 * 
 * Fragment shader from glfx.js (http://evanw.github.com/glfx.js/) by Evan Wallace.
 * 
 * @filter        Hexagonal Pixelate
 * @description   Renders the image using a pattern of hexagonal tiles. Tile colors
 *                are nearest-neighbor sampled from the centers of the tiles.
 * @param centerX The x coordinate of the pattern center.
 * @param centerY The y coordinate of the pattern center.
 * @param scale   The width of an individual tile, in pixels.
 */
//-- Fragment
#version 150

in vec2 TexCoord;
out vec4 FragColor;

uniform sampler2D Tex1;

uniform float scale;
uniform vec2 center;
uniform vec2 texSize;

//----------------------------
uniform bool drawCenter = true;
uniform float centerRadiusSqr = 10.0f * 10.0f;
//============================

void main() {
    vec2 coord = TexCoord * texSize - center;

    if (drawCenter && dot(coord, coord) <= centerRadiusSqr) {
        FragColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
        return;
    }

    vec2 tex = (coord) / scale;
    tex.y /= 0.866025404;
    tex.x -= tex.y * 0.5;

    vec2 a;
    if (tex.x + tex.y - floor(tex.x) - floor(tex.y) < 1.0) a = vec2(floor(tex.x), floor(tex.y));
    else a = vec2(ceil(tex.x), ceil(tex.y));
    vec2 b = vec2(ceil(tex.x), floor(tex.y));
    vec2 c = vec2(floor(tex.x), ceil(tex.y));

    vec3 TEX = vec3(tex.x, tex.y, 1.0 - tex.x - tex.y);
    vec3 A = vec3(a.x, a.y, 1.0 - a.x - a.y);
    vec3 B = vec3(b.x, b.y, 1.0 - b.x - b.y);
    vec3 C = vec3(c.x, c.y, 1.0 - c.x - c.y);

    float alen = length(TEX - A);
    float blen = length(TEX - B);
    float clen = length(TEX - C);

    vec2 choice;
    if (alen < blen) {
        if (alen < clen) choice = a;
        else choice = c;
    } else {
        if (blen < clen) choice = b;
        else choice = c;
    }

    choice.x += choice.y * 0.5;
    choice.y *= 0.866025404;
    choice *= scale / texSize;
    FragColor = texture(Tex1, choice + center / texSize);
}