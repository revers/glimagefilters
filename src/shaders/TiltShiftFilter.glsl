#include <FAKE_INCLUDE/glsl_fake.hxx>

//$END_FAKE$

//-- Vertex
#version 150

in vec3 VertexPosition;
in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition, 1.0);
}

//==================================================================================================
/**
 * 
 * Fragment shader from glfx.js (http://evanw.github.com/glfx.js/) by Evan Wallace.
 * 
 * @filter               Tilt Shift
 * @description          Simulates the shallow depth of field normally encountered in close-up
 *                       photography, which makes the scene seem much smaller than it actually
 *                       is. This filter assumes the scene is relatively planar, in which case
 *                       the part of the scene that is completely in focus can be described by
 *                       a line (the intersection of the focal plane and the scene). An example
 *                       of a planar scene might be looking at a road from above at a downward
 *                       angle. The image is then blurred with a blur radius that starts at zero
 *                       on the line and increases further from the line.
 * @param startX         The x coordinate of the start of the line segment.
 * @param startY         The y coordinate of the start of the line segment.
 * @param endX           The x coordinate of the end of the line segment.
 * @param endY           The y coordinate of the end of the line segment.
 * @param blurRadius     The maximum radius of the pyramid blur.
 * @param gradientRadius The distance from the line at which the maximum blur radius is reached.
 */
//-- Fragment
#version 150

in vec2 TexCoord;
out vec4 FragColor;

uniform sampler2D Tex1;

uniform float blurRadius;
uniform float gradientRadius;
uniform vec2 begin;
uniform vec2 end;
uniform vec2 delta;
uniform vec2 texSize;

//----------------------------
uniform bool drawControlPoints = true;
uniform float controlPointsRadiusSqr = 10.0f * 10.0f;
//============================

float random(vec3 scale, float seed) {
    /* use the fragment position for a different seed per-pixel */
    return fract(sin(dot(gl_FragCoord.xyz + seed, scale)) * 43758.5453 + seed);
}

void main() {
    vec4 color = vec4(0.0);
    float total = 0.0;

    if (drawControlPoints) {
        vec2 toBegin = begin - TexCoord * texSize;

        if (dot(toBegin, toBegin) <= controlPointsRadiusSqr) {
            FragColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
            return;
        }

        vec2 toEnd = end - TexCoord * texSize;
        if (dot(toEnd, toEnd) <= controlPointsRadiusSqr) {
            FragColor = vec4(0.0f, 0.0f, 1.0f, 1.0f);
            return;
        }
    }

    /* randomize the lookup values to hide the fixed number of samples */
    float offset = random(vec3(12.9898, 78.233, 151.7182), 0.0);

    vec2 normal = normalize(vec2(begin.y - end.y, end.x - begin.x));
    float radius = smoothstep(0.0, 1.0, abs(dot(TexCoord * texSize - begin, normal)) / gradientRadius) * blurRadius;
    for (float t = -30.0; t <= 30.0; t++) {
        float percent = (t + offset - 0.5) / 30.0;
        float weight = 1.0 - abs(percent);
        vec4 sample = texture(Tex1, TexCoord + delta / texSize * percent * radius);

        /* switch to pre-multiplied alpha to correctly blur transparent images */
        sample.rgb *= sample.a;

        color += sample * weight;
        total += weight;
    }

    FragColor = color / total;

    /* switch back from pre-multiplied alpha */
    FragColor.rgb /= FragColor.a + 0.00001;
}




