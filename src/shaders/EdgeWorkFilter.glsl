
#include <FAKE_INCLUDE/glsl_fake.hxx>

//$END_FAKE$

//-- Vertex
#version 400

layout(location = 0) in vec3 VertexPosition;
layout(location = 1) in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition, 1.0);
}

/**
 * Fragment shader from glfx.js (http://evanw.github.com/glfx.js/) by Evan Wallace.
 * 
 * @filter       Edge Work
 * @description  Picks out different frequencies in the image by subtracting two
 *               copies of the image blurred with different radii.
 * @param radius The radius of the effect in pixels.
 */
//-- Fragment
#version 400

in vec2 TexCoord;

subroutine vec4 RenderPassType();
subroutine uniform RenderPassType RenderPass;

uniform sampler2D Tex1;
uniform vec2 delta;

layout(location = 0) out vec4 FragColor;

float random(vec3 scale, float seed) {
    /* use the fragment position for a different seed per-pixel */
    return fract(sin(dot(gl_FragCoord.xyz + seed, scale)) * 43758.5453 + seed);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Trick for full support in NetBeans IDE.
// I'm pretending that this is C++ file.
#ifdef FAKE_INCLUDE
#    undef subroutine
#    define subroutine(X)
#endif
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

subroutine(RenderPassType)
vec4 pass1() {
    vec2 color = vec2(0.0);
    vec2 total = vec2(0.0);

    /* randomize the lookup values to hide the fixed number of samples */
    float offset = random(vec3(12.9898, 78.233, 151.7182), 0.0);

    for (float t = -30.0; t <= 30.0; t++) {
        float percent = (t + offset - 0.5) / 30.0;
        float weight = 1.0 - abs(percent);
        vec3 sample1 = texture(Tex1, TexCoord + delta * percent).rgb;
        float average = (sample1.r + sample1.g + sample1.b) / 3.0;
        color.x += average * weight;
        total.x += weight;
        if (abs(t) < 15.0) {
            weight = weight * 2.0 - 1.0;
            color.y += average * weight;
            total.y += weight;
        }
    }
    return vec4(color / total, 0.0, 1.0);
}

subroutine(RenderPassType)
vec4 pass2() {
    vec2 color = vec2(0.0);
    vec2 total = vec2(0.0);

    /* randomize the lookup values to hide the fixed number of samples */
    float offset = random(vec3(12.9898, 78.233, 151.7182), 0.0);

    for (float t = -30.0; t <= 30.0; t++) {
        float percent = (t + offset - 0.5) / 30.0;
        float weight = 1.0 - abs(percent);
        vec2 sample1 = texture(Tex1, TexCoord + delta * percent).xy;
        color.x += sample1.x * weight;
        total.x += weight;
        if (abs(t) < 15.0) {
            weight = weight * 2.0 - 1.0;
            color.y += sample1.y * weight;
            total.y += weight;
        }
    }
    float c = clamp(10000.0 * (color.y / total.y - color.x / total.x) + 0.5, 0.0, 1.0);

    return vec4(c, c, c, 1.0);
}

void main() {
    FragColor = RenderPass();
}