#include <FAKE_INCLUDE/glsl_fake.hxx>

//$END_FAKE$

//-- Vertex
#version 150

in vec3 VertexPosition;
in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition, 1.0);
}

/**
 * 
 * Fragment shader from glfx.js (http://evanw.github.com/glfx.js/) by Evan Wallace.
 * 
 * @filter        Dot Screen
 * @description   Simulates a black and white halftone rendering of the image by multiplying
 *                pixel values with a rotated 2D sine wave pattern.
 * @param centerX The x coordinate of the pattern origin.
 * @param centerY The y coordinate of the pattern origin.
 * @param angle   The rotation of the pattern in radians.
 * @param size    The diameter of a dot in pixels.
 */
//-- Fragment
#version 150

in vec2 TexCoord;
out vec4 FragColor;

uniform sampler2D Tex1;

uniform float angle;
uniform float scale;
uniform vec2 center;
uniform vec2 texSize;

//----------------------------
uniform bool drawCenter = true;
uniform float centerRadiusSqr = 10.0f * 10.0f;
//============================

float pattern(vec2 tex) {
    float s = sin(angle), c = cos(angle);
    //vec2 tex = TexCoord * texSize - center;
    vec2 point = vec2(
            c * tex.x - s * tex.y,
            s * tex.x + c * tex.y
            ) * scale;
    return (sin(point.x) * sin(point.y)) * 4.0;
}

void main() {
    vec2 coord = TexCoord * texSize - center;

    if (drawCenter && dot(coord, coord) <= centerRadiusSqr) {
        FragColor = vec4(1.0f, 0.0f, 0.0f, 1.0f);
        return;
    }

    vec4 color = texture(Tex1, TexCoord);
    float average = (color.r + color.g + color.b) / 3.0;
    FragColor = vec4(vec3(average * 10.0 - 5.0 + pattern(coord)), color.a);
}

