#include <FAKE_INCLUDE/glsl_fake.hxx>

//$END_FAKE$

//-- Vertex
#version 400

layout(location = 0) in vec3 VertexPosition;
layout(location = 1) in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition, 1.0);
}

/**
 * 
 * Fragment shader from glfx.js (http://evanw.github.com/glfx.js/) by Evan Wallace.
 * 
 * @filter           Lens Blur
 * @description      Imitates a camera capturing the image out of focus by using a blur that generates
 *                   the large shapes known as bokeh. The polygonal shape of real bokeh is due to the
 *                   blades of the aperture diaphragm when it isn't fully open. This blur renders
 *                   bokeh from a 6-bladed diaphragm because the computation is more efficient. It
 *                   can be separated into three rhombi, each of which is just a skewed box blur.
 *                   This filter makes use of the floating point texture WebGL extension to implement
 *                   the brightness parameter, so there will be severe visual artifacts if brightness
 *                   is non-zero and the floating point texture extension is not available. The
 *                   idea was from John White's SIGGRAPH 2011 talk but this effect has an additional
 *                   brightness parameter that fakes what would otherwise come from a HDR source.
 * @param radius     the radius of the hexagonal disk convolved with the image
 * @param brightness -1 to 1 (the brightness of the bokeh, negative values will create dark bokeh)
 * @param angle      the rotation of the bokeh in radians
 */
//==============================================================================
//-- Fragment
#version 400

in vec2 TexCoord;

subroutine vec4 RenderPassType();
subroutine uniform RenderPassType RenderPass;

uniform sampler2D Tex1;
uniform sampler2D Tex2;
uniform vec2 delta0;
uniform vec2 delta1;
uniform float power;

layout(location = 0) out vec4 FragColor;

float random(vec3 scale, float seed) {
    /* use the fragment position for a different seed per-pixel */
    return fract(sin(dot(gl_FragCoord.xyz + seed, scale)) * 43758.5453 + seed);
}

vec4 mySample(vec2 delta) {
    /* randomize the lookup values to hide the fixed number of samples */
    float offset = random(vec3(delta, 151.7182), 0.0);

    vec4 color = vec4(0.0);
    float total = 0.0;
    for (float t = 0.0; t <= 30.0; t++) {
        float percent = (t + offset) / 30.0;
        color += texture(Tex1, TexCoord + delta * percent);
        total += 1.0;
    }
    return color / total;
}

// Trick for full support in NetBeans IDE.
// I'm pretending that this is C++ file.
#ifdef FAKE_INCLUDE
#undef subroutine
#define subroutine(X)
#endif

subroutine(RenderPassType)
vec4 pass1() {
    vec4 color = texture(Tex1, TexCoord);
    color = pow(color, vec4(power));
    return color;
}

subroutine(RenderPassType)
vec4 pass2() {
    return mySample(delta0);
}

subroutine(RenderPassType)
vec4 pass3() {
    return (mySample(delta0) + mySample(delta1)) * 0.5;
}

subroutine(RenderPassType)
vec4 pass4() {
    vec4 color = (mySample(delta0) + 2.0 * texture(Tex2, TexCoord)) / 3.0;
    return pow(color, vec4(power));
}

void main() {
    FragColor = RenderPass();
}


