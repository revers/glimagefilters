#include <FAKE_INCLUDE/glsl_fake.hxx>

//$END_FAKE$

//-- Vertex
#version 150

in vec3 VertexPosition;
in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition, 1.0);
}

/**
 * Fragment shader from "OpenGL 4.0 Shading Language Cookbook" by David Wolff.
 */
//-- Fragment
#version 150

uniform float EdgeThreshold;
uniform int Width;
uniform int Height;

in vec2 TexCoord;

uniform sampler2D Tex1;

out vec4 FragColor;

float luminance(vec3 color) {
    return 0.2126 * color.r + 0.7152 * color.g + 0.0722 * color.b;
}

void main() {
    float dx = 1.0 / float(Width);
    float dy = 1.0 / float(Height);

    float s00 = luminance(texture(Tex1, TexCoord + vec2(-dx, dy)).rgb);
    float s10 = luminance(texture(Tex1, TexCoord + vec2(-dx, 0.0)).rgb);
    float s20 = luminance(texture(Tex1, TexCoord + vec2(-dx, -dy)).rgb);
    float s01 = luminance(texture(Tex1, TexCoord + vec2(0.0, dy)).rgb);
    float s21 = luminance(texture(Tex1, TexCoord + vec2(0.0, -dy)).rgb);
    float s02 = luminance(texture(Tex1, TexCoord + vec2(dx, dy)).rgb);
    float s12 = luminance(texture(Tex1, TexCoord + vec2(dx, 0.0)).rgb);
    float s22 = luminance(texture(Tex1, TexCoord + vec2(dx, -dy)).rgb);

    float sx = s00 + 2 * s10 + s20 - (s02 + 2 * s12 + s22);
    float sy = s00 + 2 * s01 + s02 - (s20 + 2 * s21 + s22);

    float dist = sx * sx + sy * sy;

    if (dist > EdgeThreshold)
        FragColor = vec4(1.0);
    else
        FragColor = vec4(0.0, 0.0, 0.0, 1.0);
}


