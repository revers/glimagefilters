
#include <FAKE_INCLUDE/glsl_fake.hxx>

//$END_FAKE$

//-- Vertex
#version 150

in vec3 VertexPosition;
in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition, 1.0);
}

/**
 * 
 * Fragment shader from "Shader Toy"  by Inigo Quilez and others 
 * (http://www.iquilezles.org/apps/shadertoy/).
 * 
 */
//-- Fragment
#version 150

in vec2 TexCoord;

uniform sampler2D Tex1;
uniform float time;

out vec4 FragColor;

void main() {
    vec2 q = TexCoord;
    //q.y = -q.y;//gl_FragCoord.xy / resolution.xy;
    vec2 uv = 0.5 + (q - 0.5) * (0.9 + 0.1 * sin(0.2 * time));

    vec3 oricol = texture(Tex1, vec2(q.x, q.y)).xyz;
    vec3 col;

    col.r = texture(Tex1, vec2(uv.x + 0.003, uv.y)).x;
    col.g = texture(Tex1, vec2(uv.x + 0.000, uv.y)).y;
    col.b = texture(Tex1, vec2(uv.x - 0.003, uv.y)).z;

    col = clamp(col * 0.5 + 0.5 * col * col * 1.2, 0.0, 1.0);

    col *= 0.5 + 0.5 * 16.0 * uv.x * uv.y * (1.0 - uv.x)*(1.0 - uv.y);

    col *= vec3(0.8, 1.0, 0.7);

    col *= 0.9 + 0.1 * sin(10.0 * time + uv.y * 1000.0);

    col *= 0.97 + 0.03 * sin(110.0 * time);

    //float comp = smoothstep(0.2, 0.7, sin(time));
    float comp = 0.0;
    
    col = mix(col, oricol, clamp(-2.0 + 2.0 * q.x + 3.0 * comp, 0.0, 1.0));

    FragColor = vec4(col, 1.0);
}