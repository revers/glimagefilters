/* 
 * File:   AllFilters.h
 * Author: Revers
 *
 * Created on 29 maj 2012, 08:00
 */

#ifndef ALLFILTERS_H
#define	ALLFILTERS_H

#include "Filters/AbstractFilter.h"
#include "Filters/BrightnessContrastFilter.h"
#include "Filters/HueSaturationFilter.h"
#include "Filters/DenoiseFilter.h"
#include "Filters/NoiseFilter.h"
#include "Filters/SepiaFilter.h"
#include "Filters/VibranceFilter.h"
#include "Filters/VignetteFilter.h"
#include "Filters/EdgeDetectionFilter.h"
#include "Filters/GaussianBlurFilter.h"
#include "Filters/TriangleBlurFilter.h"
#include "Filters/UnsharpMaskFilter.h"
#include "Filters/ZoomBlurFilter.h"
#include "Filters/TiltShiftFilter.h"
#include "Filters/BulgePinchFilter.h"
#include "Filters/SwirlFilter.h"
#include "Filters/InkFilter.h"
#include "Filters/HexagonalPixelateFilter.h"
#include "Filters/DotScreenFilter.h"
#include "Filters/ColorHalftoneFilter.h"
#include "Filters/EdgeWorkFilter.h"
#include "Filters/LensBlurFilter.h"
#include "Filters/RadialBlurFilter.h"
#include "Filters/CameraFilter.h"
#include "Filters/BloomFilter.h"
#include "Filters/WaterRippleFilter.h"

#endif	/* ALLFILTERS_H */

